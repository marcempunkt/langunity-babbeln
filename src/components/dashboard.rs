use egui::{Context, Ui};

pub struct Dashboard {}

impl Dashboard {
    pub fn new() -> Self {
        Self {}
    }

    pub fn render(&mut self, ui: &mut Ui, _ctx: &Context) {
        egui::TopBottomPanel::top("dashboard_top_panel").show_inside(ui, |ui: &mut Ui| {
            ui.horizontal(|ui: &mut Ui| {
                ui.label("Dashboard");
            });
        });

        egui::CentralPanel::default().show_inside(ui, |ui: &mut Ui| {
            ui.label("content");
        });
    }
}
