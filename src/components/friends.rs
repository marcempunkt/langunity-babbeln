use crate::components::{
    add_friend::AddFriend, blocked_users::BlockedUsers, friendrequests::Friendrequests,
    my_friends::MyFriends,
};
use egui::{Context, Ui};
use log::info;

pub struct Friends {
    my_friends: MyFriends,
    friendrequests: Friendrequests,
    blocked_users: BlockedUsers,
    add_friend: AddFriend,
    show_friends: bool,
    show_friendrequests: bool,
    show_blocked_users: bool,
    show_add_friend: bool,
}

impl Friends {
    pub fn new() -> Self {
        Self {
            my_friends: MyFriends::new(),
            friendrequests: Friendrequests::new(),
            blocked_users: BlockedUsers::new(),
            add_friend: AddFriend::new(),
            show_friends: true,
            show_friendrequests: false,
            show_blocked_users: false,
            show_add_friend: false,
        }
    }

    pub fn render(&mut self, ui: &mut Ui, ctx: &Context) {
        egui::TopBottomPanel::top("friends_top_panel").show_inside(ui, |ui: &mut Ui| {
            ui.horizontal(|ui: &mut Ui| {
                if ui.button("friends").clicked() {
                    self.show_friends();
                }

                if ui.button("friendrequests").clicked() {
                    self.show_friendrequests();
                }

                if ui.button("blocked users").clicked() {
                    self.show_blocked_users();
                }

                if ui.button("add friend").clicked() {
                    self.show_add_friend();
                }
            })
        });

        egui::CentralPanel::default().show_inside(ui, |ui: &mut Ui| {
            if self.show_friends {
                self.my_friends.render(ui, ctx);
            } else if self.show_friendrequests {
                self.friendrequests.render(ui, ctx);
            } else if self.show_blocked_users {
                self.blocked_users.render(ui, ctx);
            } else if self.show_add_friend {
                self.add_friend.render(ui, ctx);
            }
        });
    }

    fn show_friends(&mut self) {
        info!("Friends friends button clicked");
        self.show_friends = true;
        self.show_friendrequests = false;
        self.show_blocked_users = false;
        self.show_add_friend = false;
    }

    fn show_friendrequests(&mut self) {
        info!("Friends friendrequests button clicked");
        self.show_friends = false;
        self.show_friendrequests = true;
        self.show_blocked_users = false;
        self.show_add_friend = false;
    }

    fn show_blocked_users(&mut self) {
        info!("Friends blocked users button clicked");
        self.show_friends = false;
        self.show_friendrequests = false;
        self.show_blocked_users = true;
        self.show_add_friend = false;
    }

    fn show_add_friend(&mut self) {
        info!("Friends add friend button clicked");
        self.show_friends = false;
        self.show_friendrequests = false;
        self.show_blocked_users = false;
        self.show_add_friend = true;
    }
}
