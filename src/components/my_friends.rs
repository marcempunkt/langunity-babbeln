use crate::actors::{
    app_actor::use_app_handle,
    blocked_users_actor::{use_blocked_users_handle, BlockedUsersHandle},
    friends_actor::{use_friends_handle, FriendsHandle},
};
use crate::components::friend_item::FriendItem;
use crate::core::models::User;
use crate::core::task::spawn_blocking;
use crate::core::utils::handle_oneshot_receiver;
use egui::{Context, Ui};
use log::info;
use tokio::sync::oneshot;

pub struct MyFriends {
    friends_handle: FriendsHandle,
    blocked_users_handle: BlockedUsersHandle,
    friend_items: Vec<FriendItem>,
    get_friend_items: Option<oneshot::Receiver<Vec<FriendItem>>>,
}

impl MyFriends {
    pub fn new() -> Self {
        Self {
            friends_handle: use_friends_handle(),
            blocked_users_handle: use_blocked_users_handle(),
            friend_items: Vec::new(),
            get_friend_items: None,
        }
    }

    pub fn render(&mut self, ui: &mut Ui, ctx: &Context) {
        self.handle_update_friend_items();

        if self.friend_items.len() > 0 {
            egui::ScrollArea::vertical()
                .max_width(std::f32::INFINITY)
                .max_height(std::f32::INFINITY)
                .auto_shrink([false, false])
                .show_viewport(ui, |ui: &mut Ui, _| {
                    for friend_item in &mut self.friend_items {
                        friend_item.render(ui, ctx);
                    }
                });
        } else {
            ui.label("no friends ;(");
        }
    }

    fn handle_update_friend_items(&mut self) {
        if self.friends_handle.friends.has_changed().unwrap()
            || self
                .blocked_users_handle
                .blocked_users
                .has_changed()
                .unwrap()
        {
            info!("Friends changed");
            self.build_friend_items();
        }

        handle_oneshot_receiver(&mut self.get_friend_items, |data: Vec<FriendItem>| {
            self.friend_items = data
        });
    }

    fn build_friend_items(&mut self) {
        info!("Rebuild friend_items");
        let (sender, receiver) = oneshot::channel::<Vec<FriendItem>>();
        // IMPORTANT: fetch blocked users to make it "seen"
        let _blocked_users = self.blocked_users_handle.get_blocked_users();
        let friends = self.friends_handle.get_friends_filtered_blocked();
        spawn_blocking(|| {
            let friend_items = friends
                .into_iter()
                .map(|user: User| FriendItem::new(user))
                .collect();
            let _ = sender.send(friend_items);
            use_app_handle().request_repaint();
        });

        self.get_friend_items = Some(receiver);
    }
}
