use crate::actors::{
    app_actor::use_app_handle,
    http_client_actor::{use_http_client_handle, HttpClientHandle, HttpError},
};
use crate::core::responses::RegisterResponse;
use crate::core::task::spawn;
use egui::{Context, Ui};
use log::{error, info};
use reqwest::Method;
use serde_json::json;
use std::cell::RefCell;
use std::rc::Rc;
use tokio::sync::oneshot;

pub struct Register {
    http_client_handle: HttpClientHandle,
    email: String,
    username: String,
    password: String,
    second_password: String,
    register_response: Option<oneshot::Receiver<Result<RegisterResponse, HttpError>>>,
    error_message: Option<String>,
    show_login: Rc<RefCell<bool>>,
}

impl Register {
    pub fn new(show_login: Rc<RefCell<bool>>) -> Self {
        Self {
            http_client_handle: use_http_client_handle(),
            email: String::new(),
            username: String::new(),
            password: String::new(),
            second_password: String::new(),
            register_response: None,
            error_message: None,
            show_login,
        }
    }

    pub fn render(&mut self, ui: &mut Ui, _ctx: &Context) {
        self.handle_register_response();

        ui.label("email");
        ui.add(egui::TextEdit::singleline(&mut self.email).hint_text("email"));
        ui.label("username");
        ui.add(egui::TextEdit::singleline(&mut self.username).hint_text("username"));
        ui.label("password");
        ui.add(
            egui::TextEdit::singleline(&mut self.password)
                .hint_text("password")
                .password(true),
        );
        ui.label("retype password");
        ui.add(
            egui::TextEdit::singleline(&mut self.second_password)
                .hint_text("retype password")
                .password(true),
        );

        if ui.button("register").clicked() {
            self.handle_register();
        }

        if let Some(error_message) = self.error_message.clone() {
            ui.label(&error_message);
        }
    }

    fn handle_register_response(&mut self) {
        HttpClientHandle::handle_response_data(
            &mut self.register_response,
            |err: HttpError| {
                error!("Register request error: {:?}", err);
                self.error_message = Some(err.message);
            },
            |_data: RegisterResponse| {
                info!("Register request was successfull");
                self.email = String::new();
                self.username = String::new();
                self.password = String::new();
                self.second_password = String::new();
                // self.error_message = None;
                *self.show_login.borrow_mut() = true;
            },
        );
    }

    fn handle_register(&mut self) {
        info!("Register fired");
        let (sender, receiver) = oneshot::channel::<Result<RegisterResponse, HttpError>>();
        let username: String = self.username.clone();
        let email: String = self.email.clone();
        let password: String = self.password.clone();

        HttpClientHandle::do_request(
            Method::POST,
            "/users",
            Some(json!({
                "username": username,
                "email": email,
                "password": password,
            })),
            None,
            sender,
        );

        self.register_response = Some(receiver);
    }
}
