use crate::actors::{
    app_actor::use_app_handle,
    http_client_actor::{use_http_client_handle, HttpClientHandle, HttpError},
};
use crate::core::responses::LoginResponse;
use crate::core::task::spawn;
use egui::{Context, Ui};
use log::{error, info};
use reqwest::Method;
use serde_json::json;
use tokio::sync::oneshot;

pub struct Login {
    http_client_handle: HttpClientHandle,
    email: String,
    password: String,
    login_response: Option<oneshot::Receiver<Result<LoginResponse, HttpError>>>,
    error_message: Option<String>,
}

impl Login {
    pub fn new() -> Self {
        Self {
            http_client_handle: use_http_client_handle(),
            email: String::new(),
            password: String::new(),
            login_response: None,
            error_message: None,
        }
    }

    pub fn render(&mut self, ui: &mut Ui, _ctx: &Context) {
        self.handle_login_response();

        ui.label("email");
        ui.add(egui::TextEdit::singleline(&mut self.email).hint_text("email"));

        ui.label("password");
        ui.add(
            egui::TextEdit::singleline(&mut self.password)
                .hint_text("password")
                .password(true),
        );

        if ui.button("login").clicked() {
            self.handle_login();
        }

        if let Some(error_message) = self.error_message.clone() {
            ui.label(&error_message);
        }
    }

    fn handle_login_response(&mut self) {
        HttpClientHandle::handle_response_data(
            &mut self.login_response,
            |err: HttpError| {
                error!("Login request error: {:?}", err);
                self.error_message = Some(err.message);
            },
            |data: LoginResponse| {
                info!("Login request was successfull");
                self.email = String::new();
                self.password = String::new();
                // self.error_message = None;
                self.http_client_handle.set_access_token(Some(data.access));
                self.http_client_handle
                    .set_refresh_token(Some(data.refresh));
            },
        );
    }

    fn handle_login(&mut self) {
        info!("Login fired");
        let (sender, receiver) = oneshot::channel::<Result<LoginResponse, HttpError>>();
        let email: String = self.email.clone();
        let password: String = self.password.clone();

        HttpClientHandle::do_request(
            Method::POST,
            "/token/",
            Some(json!({
                "email": email,
                "password": password,
            })),
            None,
            sender,
        );

        self.login_response = Some(receiver);
    }
}
