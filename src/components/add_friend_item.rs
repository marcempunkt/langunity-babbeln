use crate::actors::{
    app_actor::use_app_handle,
    friendrequests_actor::use_friendrequests_handle,
    http_client_actor::{use_http_client_handle, HttpClientHandle, HttpError},
};
use crate::core::models::User;
use crate::core::notification::{create_toast, NotificationKind};
use crate::core::responses::{GetUsersResponse, SendFriendrequestResponse};
use crate::core::task::spawn;
use egui::{Context, Ui};
use log::{error, info, warn};
use reqwest::Method;
use serde_json::json;
use tokio::sync::oneshot;

pub struct AddFriendItem {
    http_client_handle: HttpClientHandle,
    user: User,
    send_friendrequest_response:
        Option<oneshot::Receiver<Result<SendFriendrequestResponse, HttpError>>>,
}

impl AddFriendItem {
    pub fn new(user: User) -> Self {
        Self {
            http_client_handle: use_http_client_handle(),
            user,
            send_friendrequest_response: None,
        }
    }

    pub fn render(&mut self, ui: &mut Ui, ctx: &Context) {
        self.handle_responses();

        ui.group(|ui: &mut Ui| {
            ui.horizontal(|ui: &mut Ui| {
                ui.label(&self.user.username);

                if ui.button("show profile").clicked() {
                    warn!("todo show profile clicked");
                }

                if ui.button("add").clicked() {
                    self.send_friendrequest(self.user.id);
                }
            });
        });
    }

    fn send_friendrequest(&mut self, user_id: usize) {
        info!("Send friendrequest button clicked");
        let (sender, receiver) = oneshot::channel::<Result<SendFriendrequestResponse, HttpError>>();

        HttpClientHandle::do_authenticated_request(
            Method::POST,
            "/friendrequests/",
            Some(json!({
                    "to_user": user_id,
            })),
            None,
            sender,
        );

        self.send_friendrequest_response = Some(receiver);
    }

    fn handle_responses(&mut self) {
        HttpClientHandle::handle_response_data(
            &mut self.send_friendrequest_response,
            |err: HttpError| {
                error!("Send friendrequest request error: {:?}", err);
                create_toast(NotificationKind::Error, err.message)
            },
            |data: SendFriendrequestResponse| {
                info!("Send friendrequest request was successfull");
                use_friendrequests_handle().fetch();
                create_toast(NotificationKind::Success, "Send friendrequest")
            },
        );
    }
}
