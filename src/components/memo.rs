use crate::actors::{
    app_actor::{use_app_handle, AppHandle},
    messages_actor::{use_messages_handle, MessagesHandle},
    user_actor::{use_user_handle, UserHandle},
};
use crate::core::models::User;
use egui::{Context, Sense, Ui};
use log::{error, info};

pub struct Memo {
    app_handle: AppHandle,
    messages_handle: MessagesHandle,
    user_handle: UserHandle,
    me: Option<User>,
}

impl Memo {
    pub fn new() -> Self {
        let mut user_handle = use_user_handle();
        let me: Option<User> = user_handle.get_me();

        Self {
            app_handle: use_app_handle(),
            messages_handle: use_messages_handle(),
            user_handle,
            me,
        }
    }

    pub fn render(&mut self, ui: &mut Ui, ctx: &Context) {
        self.handle_update_me();

        let is_active: bool = {
            let chat_partner_id: usize = self.app_handle.get_show_chat().unwrap_or(0);

            match &self.me {
                Some(me) => me.id == chat_partner_id ,
                None => false,
            }
        };

        ui.group(|ui: &mut Ui| {
            ui.horizontal(|ui: &mut Ui| {
                if is_active {
                    ui.label(">");
                }

                let memo = ui.add(egui::Label::new("Note to Self").sense(Sense::click()));

                if memo.clicked() && !is_active {
                    self.handle_memo_clicked();
                }

                memo.context_menu(|ui: &mut Ui| {
                    if ui.button("remove all messages").clicked() {
                        self.handle_remove_clicked();
                        ui.close_menu();
                    }
                });
            });
        });
    }

    fn handle_memo_clicked(&mut self) {
        info!("Memo button clicked");
        if let Some(me) = &self.me {
            self.app_handle.set_show_chat(me.id);
            self.messages_handle.fetch_chat(me.id);
        } else {
            error!("Me doesn't exist");
        }
    }

    fn handle_remove_clicked(&mut self) {
        error!("todo remove all messages button clicked");
    }

    fn handle_update_me(&mut self) {
        if self.user_handle.me.has_changed().unwrap() {
            info!("Me changed");
            self.me = self.user_handle.get_me();
        }
    }
}
