use crate::actors::{
    app_actor::{use_app_handle, AppHandle},
    blocked_users_actor::{use_blocked_users_handle, BlockedUsersHandle},
    friends_actor::{use_friends_handle, FriendsHandle},
    http_client_actor::{HttpClientHandle, HttpError},
    messages_actor::{use_messages_handle, MessagesHandle},
    user_actor::UserHandle,
};
use crate::core::models::User;
use crate::core::notification::{create_toast, NotificationKind};
use crate::core::responses::{BlockUserResponse, RemoveFriendResponse, UnblockUserResponse};
use crate::modals::show_profile_modal::ShowProfileModal;
use egui::{Context, Sense, Ui};
use log::{error, info};
use tokio::sync::oneshot;

pub struct DirectMessageItem {
    app_handle: AppHandle,
    blocked_users_handle: BlockedUsersHandle,
    friends_handle: FriendsHandle,
    messages_handle: MessagesHandle,
    user: User,
    username: String,
    is_blocked: bool,
    is_friend: bool,
    block_response: Option<oneshot::Receiver<Result<BlockUserResponse, HttpError>>>,
    unblock_response: Option<oneshot::Receiver<Result<UnblockUserResponse, HttpError>>>,
    remove_response: Option<oneshot::Receiver<Result<RemoveFriendResponse, HttpError>>>,
    show_profile_modal: ShowProfileModal,
}

impl DirectMessageItem {
    pub fn new(user: User) -> Self {
        let mut friends_handle = use_friends_handle();
        let mut blocked_users_handle = use_blocked_users_handle();

        let username: String = { UserHandle::display_username(&user) };
        let is_blocked: bool = blocked_users_handle.is_blocked(&user);
        let is_friend: bool = friends_handle.is_friend(&user);

        Self {
            app_handle: use_app_handle(),
            blocked_users_handle,
            friends_handle,
            messages_handle: use_messages_handle(),
            user: user.clone(),
            username,
            is_blocked,
            is_friend,
            block_response: None,
            unblock_response: None,
            remove_response: None,
            show_profile_modal: ShowProfileModal::new(user),
        }
    }

    pub fn render(&mut self, ui: &mut Ui, ctx: &Context) {
        self.handle_responses();
        self.show_profile_modal.render(ui, ctx);

        let is_active: bool = {
            let chat_partner_id: usize = self.app_handle.get_show_chat().unwrap_or(0);
            chat_partner_id == self.user.id
        };

        ui.group(|ui: &mut Ui| {
            ui.horizontal(|ui: &mut Ui| {
                if is_active {
                    ui.label(">");
                }

                let username = ui.add(egui::Label::new(&self.username).sense(Sense::click()));

                if username.clicked() && !is_active {
                    info!("DirectMessageItem button clicked");
                    self.app_handle.set_show_chat(self.user.id);
                    self.messages_handle.fetch_chat(self.user.id);
                }

                username.context_menu(|ui: &mut Ui| {
                    if ui.button("show profile").clicked() {
                        self.show_profile();
                        ui.close_menu();
                    }

                    if self.is_blocked {
                        if ui.button("unblock").clicked() {
                            self.unblock();
                            ui.close_menu();
                        }
                    } else {
                        if ui.button("block").clicked() {
                            self.block();
                            ui.close_menu();
                        }
                    }

                    if self.is_friend {
                        if ui.button("remove").clicked() {
                            self.remove();
                            ui.close_menu();
                        }
                    }
                });
            });
        });
    }

    fn show_profile(&mut self) {
        info!("Show profile button clicked");
        self.show_profile_modal.open();
    }

    fn block(&mut self) {
        info!("Block button clicked");
        let (sender, receiver) = oneshot::channel::<Result<BlockUserResponse, HttpError>>();
        BlockedUsersHandle::block_user(self.user.id, sender);
        self.block_response = Some(receiver);
    }

    fn unblock(&mut self) {
        info!("Unblock button clicked");
        let (sender, receiver) = oneshot::channel::<Result<UnblockUserResponse, HttpError>>();
        BlockedUsersHandle::unblock_user(self.user.id, sender);
        self.unblock_response = Some(receiver);
    }

    fn remove(&mut self) {
        info!("Remove button clicked");
        let (sender, receiver) = oneshot::channel::<Result<RemoveFriendResponse, HttpError>>();
        FriendsHandle::unfriend_user(self.user.id, sender);
        self.remove_response = Some(receiver);
    }

    fn handle_responses(&mut self) {
        HttpClientHandle::handle_response_data(
            &mut self.block_response,
            |err: HttpError| {
                error!("Block user request error: {:?}", err);
                create_toast(NotificationKind::Error, err.message);
            },
            |_data: BlockUserResponse| {
                info!("Block user request was successfull");
                self.blocked_users_handle.fetch();
            },
        );

        HttpClientHandle::handle_response_data(
            &mut self.unblock_response,
            |err: HttpError| {
                error!("Unblock user request error: {:?}", err);
                create_toast(NotificationKind::Error, err.message);
            },
            |_data: UnblockUserResponse| {
                info!("Unblock user request was successfull");
                self.blocked_users_handle.fetch();
            },
        );

        HttpClientHandle::handle_response_data(
            &mut self.remove_response,
            |err: HttpError| {
                error!("Remove user request error: {:?}", err);
                create_toast(NotificationKind::Error, err.message);
            },
            |_data: RemoveFriendResponse| {
                info!("Remove user request was successfull");
                self.friends_handle.fetch();
            },
        );
    }
}
