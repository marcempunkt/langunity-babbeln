use crate::actors::{
    blocked_users_actor::{use_blocked_users_handle, BlockedUsersHandle},
    friends_actor::{use_friends_handle, FriendsHandle},
    user_actor::{use_user_handle, UserHandle},
};
use crate::core::models::{Message, User};
use egui::{Context, Sense, Ui};
use log::{error, info};

pub struct MessageItem {
    blocked_users_handle: BlockedUsersHandle,
    friends_handle: FriendsHandle,
    message: Message,
    me: User,
    is_sender_me: bool,
    is_receiver_me: bool,
}

impl MessageItem {
    pub fn new(message: Message) -> Self {
        let me: User = use_user_handle().get_me().unwrap();
        let is_sender_me: bool = message.sender.contains(&format!("/users/{}", me.id));
        let is_receiver_me: bool = message.receiver.contains(&format!("/users/{}", me.id));

        Self {
            blocked_users_handle: use_blocked_users_handle(),
            friends_handle: use_friends_handle(),
            message,
            me,
            is_sender_me,
            is_receiver_me,
        }
    }

    pub fn render(&mut self, ui: &mut Ui, _ctx: &Context, chat_partner_username: &str) {
        if self.is_sender_me {
            let mut message = ui.group(|ui: &mut Ui| {
                ui.horizontal(|ui: &mut Ui| {
                    ui.label(&self.me.username);
                    ui.label(&self.message.content);
                    ui.label(format!(
                        "{}",
                        &self.message.send_at.format("%d/%m/%Y %H:%M")
                    ));
                });
            });

            message.response.sense = Sense::click();
            message.response.context_menu(|ui: &mut Ui| {
                if ui.button("copy").clicked() {
                    error!("todo copy clicked");
                    ui.close_menu();
                }

                if ui.button("edit").clicked() {
                    error!("todo edit clicked");
                    ui.close_menu();
                }

                if ui.button("delete").clicked() {
                    error!("todo delete clicked");
                    ui.close_menu();
                }
            });
        } else if self.is_receiver_me {
            let mut message = ui.group(|ui: &mut Ui| {
                ui.horizontal(|ui: &mut Ui| {
                    ui.label(chat_partner_username);
                    ui.label(&self.message.content);
                    ui.label(format!(
                        "{}",
                        &self.message.send_at.format("%d/%m/%Y %H:%M")
                    ));
                });
            });

            message.response.sense = Sense::click();
            message.response.context_menu(|ui: &mut Ui| {
                if ui.button("copy").clicked() {
                    error!("todo copy clicked");
                    ui.close_menu();
                }

                if ui.button("delete").clicked() {
                    error!("todo delete clicked");
                    ui.close_menu();
                }
            });
        }
    }
}
