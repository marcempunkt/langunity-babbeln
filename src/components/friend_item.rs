use crate::actors::{
    app_actor::{use_app_handle, AppHandle},
    blocked_users_actor::{use_blocked_users_handle, BlockedUsersHandle},
    friends_actor::{use_friends_handle, FriendsHandle},
    http_client_actor::{use_http_client_handle, HttpClientHandle, HttpError},
};
use crate::core::models::User;
use crate::core::responses::{BlockUserResponse, RemoveFriendResponse};
use crate::core::task::spawn;
use egui::{Context, Ui};
use log::{error, info};
use reqwest::Method;
use serde_json::json;
use tokio::sync::oneshot;

pub struct FriendItem {
    app_handle: AppHandle,
    friends_handle: FriendsHandle,
    blocked_users_handle: BlockedUsersHandle,
    user: User,
    unfriend_response: Option<oneshot::Receiver<Result<RemoveFriendResponse, HttpError>>>,
    block_response: Option<oneshot::Receiver<Result<BlockUserResponse, HttpError>>>,
    error_message: Option<String>,
}

impl FriendItem {
    pub fn new(user: User) -> Self {
        Self {
            app_handle: use_app_handle(),
            friends_handle: use_friends_handle(),
            blocked_users_handle: use_blocked_users_handle(),
            user,
            unfriend_response: None,
            block_response: None,
            error_message: None,
        }
    }

    pub fn render(&mut self, ui: &mut Ui, _ctx: &Context) {
        self.handle_responses();

        if let Some(error_message) = &self.error_message {
            ui.label(error_message);
        }

        ui.group(|ui: &mut Ui| {
            ui.horizontal(|ui: &mut Ui| {
                ui.label(&self.user.username);

                if ui.button("open chat").clicked() {
                    self.open_chat(self.user.id);
                }

                if ui.button("block").clicked() {
                    self.block(self.user.id);
                }

                if ui.button("unfriend").clicked() {
                    self.unfriend(self.user.id);
                }
            })
        });
    }

    fn open_chat(&mut self, user_id: usize) {
        info!("Open chat button clicked for user_id: {user_id}");
        self.app_handle.set_show_chat(user_id);
    }

    fn block(&mut self, user_id: usize) {
        info!("Block button clicked for user_id: {user_id}");
        let (sender, receiver) = oneshot::channel::<Result<BlockUserResponse, HttpError>>();
        BlockedUsersHandle::block_user(user_id, sender);
        self.block_response = Some(receiver);
    }

    fn unfriend(&mut self, user_id: usize) {
        info!("Unfriend button clicked for user_id: {user_id}");
        let (sender, receiver) = oneshot::channel::<Result<RemoveFriendResponse, HttpError>>();
        FriendsHandle::unfriend_user(user_id, sender);
        self.unfriend_response = Some(receiver);
    }

    fn handle_responses(&mut self) {
        HttpClientHandle::handle_response_data(
            &mut self.unfriend_response,
            |err: HttpError| {
                error!("Unfriend user request error: {:?}", err);
                self.error_message = Some(err.message);
            },
            |_data: RemoveFriendResponse| {
                info!("Unfriend request was successfull");
                self.friends_handle.fetch();
            },
        );

        HttpClientHandle::handle_response_data(
            &mut self.block_response,
            |err: HttpError| {
                error!("Block user request error: {:?}", err);
                self.error_message = Some(err.message);
            },
            |_data: BlockUserResponse| {
                info!("Block user request was successfull");
                self.blocked_users_handle.fetch()
            },
        );
    }
}
