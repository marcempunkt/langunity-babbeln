use crate::actors::{
    app_actor::use_app_handle,
    friendrequests_actor::use_friendrequests_handle,
    http_client_actor::{use_http_client_handle, HttpClientHandle, HttpError},
};
use crate::components::add_friend_item::AddFriendItem;
use crate::core::models::User;
use crate::core::notification::{create_toast, NotificationKind};
use crate::core::responses::{GetUsersResponse, SendFriendrequestResponse};
use crate::core::task::spawn;
use egui::{Context, Ui};
use log::{error, info};
use reqwest::Method;
use serde_json::json;
use tokio::sync::oneshot;

pub struct AddFriend {
    http_client_handle: HttpClientHandle,
    query: String,
    users: Vec<AddFriendItem>,
    get_users_response: Option<oneshot::Receiver<Result<GetUsersResponse, HttpError>>>,
}

impl AddFriend {
    pub fn new() -> Self {
        Self {
            http_client_handle: use_http_client_handle(),
            query: String::new(),
            users: Vec::new(),
            get_users_response: None,
        }
    }

    pub fn render(&mut self, ui: &mut Ui, ctx: &Context) {
        self.handle_responses();

        ui.horizontal(|ui: &mut Ui| {
            ui.text_edit_singleline(&mut self.query);

            if ui.button("search").clicked() {
                self.search_users();
            }
        });

        egui::ScrollArea::vertical()
            .max_width(std::f32::INFINITY)
            .max_height(std::f32::INFINITY)
            .auto_shrink([false, false])
            .show_viewport(ui, |ui: &mut Ui, _| {
                self.users.iter_mut().for_each(|user: &mut AddFriendItem| {
                    user.render(ui, ctx);
                })
            });
    }

    fn search_users(&mut self) {
        info!("Search users fired");
        let (sender, receiver) = oneshot::channel::<Result<GetUsersResponse, HttpError>>();
        let query: String = self.query.clone();

        HttpClientHandle::do_authenticated_request(
            Method::GET,
            "/users/",
            None,
            Some(vec![("query".into(), query.into())]),
            sender,
        );

        self.get_users_response = Some(receiver);
    }

    fn handle_responses(&mut self) {
        HttpClientHandle::handle_response_data(
            &mut self.get_users_response,
            |err: HttpError| {
                error!("Get users request error: {:?}", err);
                create_toast(NotificationKind::Error, err.message);
            },
            |data: GetUsersResponse| {
                info!("Build all AddFriendItems");
                // TODO maybe move this inside spawn_blocking for better perfomance
                self.users = data
                    .result
                    .into_iter()
                    .map(|user: User| AddFriendItem::new(user))
                    .collect()
            },
        );
    }
}
