use crate::actors::{
    app_actor::use_app_handle,
    blocked_users_actor::{use_blocked_users_handle, BlockedUsersHandle},
    friends_actor::{use_friends_handle, FriendsHandle},
    posts_actor::{use_posts_handle, PostsHandle},
};
use crate::components::post_item::PostItem;
use crate::core::models::Post;
use crate::core::task::spawn_blocking;
use crate::core::utils::handle_oneshot_receiver;
use egui::{Context, Ui};
use log::{error, info};
use tokio::sync::oneshot;

pub struct Posts {
    blocked_users_handle: BlockedUsersHandle,
    friends_handle: FriendsHandle,
    posts_handle: PostsHandle,
    post_items: Vec<PostItem>,
    get_post_items: Option<oneshot::Receiver<Vec<PostItem>>>,
}

impl Posts {
    pub fn new() -> Self {
        Self {
            blocked_users_handle: use_blocked_users_handle(),
            friends_handle: use_friends_handle(),
            posts_handle: use_posts_handle(),
            post_items: Vec::new(),
            get_post_items: None,
        }
    }

    pub fn render(&mut self, ui: &mut Ui, ctx: &Context) {
        self.handle_update_post_items();

        egui::ScrollArea::vertical()
            .max_width(std::f32::INFINITY)
            .max_height(std::f32::INFINITY)
            .auto_shrink([false, false])
            .stick_to_bottom(true)
            .show_viewport(ui, |ui: &mut Ui, _| {
                if ui.button("load more posts").clicked() {}

                for post_item in &mut self.post_items {
                    post_item.render(ui, ctx);
                }
            });
    }

    fn handle_update_post_items(&mut self) {
        if self.posts_handle.posts.has_changed().unwrap()
            || self
                .blocked_users_handle
                .blocked_users
                .has_changed()
                .unwrap()
            || self.friends_handle.friends.has_changed().unwrap()
        {
            info!("Posts changed");
            self.blocked_users_handle.get_blocked_users();
            self.friends_handle.get_friends();
            self.build_post_items();
        }

        handle_oneshot_receiver(&mut self.get_post_items, |data: Vec<PostItem>| {
            self.post_items = data
        });
    }

    fn build_post_items(&mut self) {
        info!("Build post items");
        let posts: Vec<Post> = self.posts_handle.get_posts_filtered_blocked();
        let (sender, receiver) = oneshot::channel::<Vec<PostItem>>();

        spawn_blocking(|| {
            let post_items: Vec<PostItem> = posts
                .into_iter()
                .rev()
                .map(|post: Post| PostItem::new(post))
                .collect();

            let _ = sender.send(post_items);
            use_app_handle().request_repaint();
            info!("Finished building post items");
        });

        self.get_post_items = Some(receiver);
    }
}
