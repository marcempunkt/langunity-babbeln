use crate::actors::{
    app_actor::{use_app_handle, AppHandle},
    blocked_users_actor::{use_blocked_users_handle, BlockedUsersHandle},
    chat_partners_actor::use_chat_partners_handle,
    friends_actor::{use_friends_handle, FriendsHandle},
    http_client_actor::{use_http_client_handle, HttpClientHandle, HttpError},
    messages_actor::{use_messages_handle, MessagesHandle},
    user_actor::{use_user_handle, UserHandle},
};
use crate::components::chat_messages::ChatMessages;
use crate::core::models::User;
use crate::core::notification::{create_toast, NotificationKind};
use crate::core::responses::SendMessageResponse;
use crate::core::task::spawn;
use egui::{Context, Ui};
use log::{error, info, warn};
use reqwest::Method;
use serde_json::json;
use tokio::sync::oneshot;

pub struct Chat {
    app_handle: AppHandle,
    blocked_users_handle: BlockedUsersHandle,
    friends_handle: FriendsHandle,
    http_client_handle: HttpClientHandle,
    messages_handle: MessagesHandle,
    user_handle: UserHandle,
    chat_messages: ChatMessages,
    input_message: String,
    send_response: Option<oneshot::Receiver<Result<SendMessageResponse, HttpError>>>,
    title: String,
    chat_partner_username: String,
}

impl Chat {
    pub fn new() -> Self {
        Self {
            app_handle: use_app_handle(),
            blocked_users_handle: use_blocked_users_handle(),
            friends_handle: use_friends_handle(),
            http_client_handle: use_http_client_handle(),
            messages_handle: use_messages_handle(),
            user_handle: use_user_handle(),
            chat_messages: ChatMessages::new(),
            input_message: String::new(),
            send_response: None,
            title: String::new(),
            chat_partner_username: String::new(),
        }
    }

    pub fn render(&mut self, ui: &mut Ui, ctx: &Context) {
        self.handle_responses();
        self.handle_update_title();

        egui::TopBottomPanel::top("chat_top_panel").show_inside(ui, |ui: &mut Ui| {
            ui.horizontal(|ui: &mut Ui| {
                ui.label(&self.title);

                if ui.button("audio call").clicked() {
                    warn!("todo audio call btn clicked");
                }

                if ui.button("video call").clicked() {
                    warn!("todo video call btn clicked");
                }

                if ui.button("search").clicked() {
                    warn!("todo search btn clicked");
                }
            });
        });

        egui::TopBottomPanel::bottom("chat_bottom_panel").show_inside(ui, |ui: &mut Ui| {
            egui::SidePanel::right("chat_bottom_panel_side_panel").show_inside(
                ui,
                |ui: &mut Ui| {
                    if ui.button("send").clicked() {
                        self.send();
                    }
                },
            );

            egui::CentralPanel::default().show_inside(ui, |ui: &mut Ui| {
                ui.add(
                    egui::TextEdit::multiline(&mut self.input_message)
                        .hint_text("Message")
                        .desired_width(std::f32::INFINITY)
                        .desired_rows(1)
                        .cursor_at_end(true),
                );
            });
        });

        egui::CentralPanel::default().show_inside(ui, |ui: &mut Ui| {
            self.chat_messages
                .render(ui, ctx, &self.chat_partner_username);
        });
    }

    fn handle_responses(&mut self) {
        HttpClientHandle::handle_response_data(
            &mut self.send_response,
            |err: HttpError| {
                error!("Send message request error: {:?}", err);
                create_toast(NotificationKind::Error, err.message);
            },
            |_data: SendMessageResponse| {
                info!("Send message request was successfull");
                let chat_partner_id: usize = self.app_handle.get_show_chat().unwrap();
                self.messages_handle.fetch_chat(chat_partner_id);
                use_chat_partners_handle().set_latest_chat_partner(chat_partner_id);
            },
        );
    }

    fn handle_update_title(&mut self) {
        if self.app_handle.show_chat.has_changed().unwrap()
            || self
                .blocked_users_handle
                .blocked_users
                .has_changed()
                .unwrap()
            || self.friends_handle.friends.has_changed().unwrap()
        {
            info!("Chat title updated");
            self.blocked_users_handle.get_blocked_users();
            self.friends_handle.get_friends();

            let chat_partner_id: usize = self.app_handle.get_show_chat().unwrap();
            let me: User = self.user_handle.get_me().unwrap();
            let is_memo: bool = chat_partner_id == me.id;

            if is_memo {
                self.title = format!("Note to Self");
                self.chat_partner_username = me.username;
            } else {
                let username: String =
                    UserHandle::display_username_by_id(self.app_handle.get_show_chat().unwrap());
                self.title = format!("@{username}");
                self.chat_partner_username = username;
            }
        }
    }

    fn send(&mut self) {
        info!("Send message button was clicked");
        let (sender, receiver) = oneshot::channel::<Result<SendMessageResponse, HttpError>>();
        let to_user: usize = self.app_handle.get_show_chat().unwrap();
        let content: String = self.input_message.clone();

        HttpClientHandle::do_authenticated_request(
            Method::POST,
            "/messages/",
            Some(json!({
                "to_user": to_user,
                "content": content,
            })),
            None,
            sender,
        );

        self.send_response = Some(receiver);
    }
}
