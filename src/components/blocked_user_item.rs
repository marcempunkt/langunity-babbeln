use crate::actors::{
    app_actor::use_app_handle,
    blocked_users_actor::{use_blocked_users_handle, BlockedUsersHandle},
    http_client_actor::{use_http_client_handle, HttpClientHandle, HttpError},
};
use crate::core::models::User;
use crate::core::responses::UnblockUserResponse;
use crate::core::task::spawn;
use egui::{Context, Ui};
use log::{error, info};
use reqwest::Method;
use tokio::sync::oneshot;

pub struct BlockedUserItem {
    blocked_users_handle: BlockedUsersHandle,
    blocked_user: User,
    unblock_response: Option<oneshot::Receiver<Result<UnblockUserResponse, HttpError>>>,
    error_message: Option<String>,
}

impl BlockedUserItem {
    pub fn new(blocked_user: User) -> Self {
        Self {
            blocked_users_handle: use_blocked_users_handle(),
            blocked_user,
            unblock_response: None,
            error_message: None,
        }
    }

    pub fn render(&mut self, ui: &mut Ui, _ctx: &Context) {
        self.handle_responses();

        if let Some(error_message) = &self.error_message {
            ui.label(error_message);
        }

        ui.group(|ui: &mut Ui| {
            ui.horizontal(|ui: &mut Ui| {
                ui.label(&self.blocked_user.username);

                if ui.button("unblock").clicked() {
                    self.unblock(self.blocked_user.id);
                }
            });
        });
    }

    fn unblock(&mut self, user_id: usize) {
        info!("Unblock button clicked for user with the id: {user_id}");
        let (sender, receiver) = oneshot::channel::<Result<UnblockUserResponse, HttpError>>();
        BlockedUsersHandle::unblock_user(user_id, sender);
        self.unblock_response = Some(receiver);
    }

    fn handle_responses(&mut self) {
        HttpClientHandle::handle_response_data(
            &mut self.unblock_response,
            |err: HttpError| {
                error!("Unblock user request error: {:?}", err);
                self.error_message = Some(err.message);
            },
            |_data: UnblockUserResponse| {
                info!("Unblock user request was successfull");
                self.blocked_users_handle.fetch();
            },
        );
    }
}
