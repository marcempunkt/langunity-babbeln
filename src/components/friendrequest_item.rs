use crate::actors::{
    app_actor::use_app_handle,
    friendrequests_actor::{use_friendrequests_handle, FriendrequestsHandle},
    friends_actor::{use_friends_handle, FriendsHandle},
    http_client_actor::{use_http_client_handle, HttpClientHandle, HttpError},
    user_actor::{use_user_handle, UserHandle},
};
use crate::core::models::{Friendrequest, User};
use crate::core::responses::{
    AcceptFriendrequestResponse, DeclineFriendrequestResponse, DeleteFriendrequestResponse,
    GetUserResponse,
};
use crate::core::task::spawn;
use egui::{Context, Ui};
use log::{error, info};
use reqwest::Method;
use tokio::sync::oneshot;

pub struct FriendrequestItem {
    friendrequests_handle: FriendrequestsHandle,
    friends_handle: FriendsHandle,
    user_handle: UserHandle,
    friendrequest: Friendrequest,
    friendrequest_user: Option<User>,
    get_user_response: Option<oneshot::Receiver<Result<GetUserResponse, HttpError>>>,
    accept_response: Option<oneshot::Receiver<Result<AcceptFriendrequestResponse, HttpError>>>,
    decline_response: Option<oneshot::Receiver<Result<DeclineFriendrequestResponse, HttpError>>>,
    cancel_response: Option<oneshot::Receiver<Result<DeleteFriendrequestResponse, HttpError>>>,
    error_message: Option<String>,
}

impl FriendrequestItem {
    pub fn new(friendrequest: Friendrequest) -> Self {
        Self {
            friendrequests_handle: use_friendrequests_handle(),
            friends_handle: use_friends_handle(),
            user_handle: use_user_handle(),
            friendrequest,
            friendrequest_user: None,
            get_user_response: None,
            accept_response: None,
            decline_response: None,
            cancel_response: None,
            error_message: None,
        }
    }

    pub fn render(&mut self, ui: &mut Ui, _ctx: &Context) {
        self.handle_responses();

        if let Some(error_message) = &self.error_message {
            ui.label(error_message);
        }

        ui.group(|ui: &mut Ui| {
            ui.horizontal(|ui: &mut Ui| {
                let me: User = self.user_handle.get_me().unwrap();
                let is_friendrequest_from_me: bool = self
                    .friendrequest
                    .from_user
                    .contains(&format!("/users/{}", me.id));
                let other_user: String = match is_friendrequest_from_me {
                    true => self.friendrequest.to_user.clone(),
                    false => self.friendrequest.from_user.clone(),
                };

                if let Some(user) = &self.friendrequest_user {
                    ui.label(&user.username);
                } else if self.get_user_response.is_none() {
                    self.get_user(other_user);
                }

                if is_friendrequest_from_me {
                    ui.label("pending ...");

                    if ui.button("cancel").clicked() {
                        self.cancel(self.friendrequest.id);
                    }
                } else {
                    if ui.button("accept").clicked() {
                        self.accept(self.friendrequest.id)
                    }

                    if ui.button("decline").clicked() {
                        self.decline(self.friendrequest.id);
                    }
                }
            })
        });
    }

    fn get_user(&mut self, user_route: String) {
        info!("Get user fired for: {user_route}");
        let (sender, receiver) = oneshot::channel::<Result<GetUserResponse, HttpError>>();

        HttpClientHandle::do_authenticated_request(Method::GET, user_route, None, None, sender);

        self.get_user_response = Some(receiver);
    }

    fn accept(&mut self, friendrequest_id: usize) {
        info!("Accept friendrequest fired for id: {friendrequest_id}");
        let (sender, receiver) =
            oneshot::channel::<Result<AcceptFriendrequestResponse, HttpError>>();

        HttpClientHandle::do_authenticated_request(
            Method::PATCH,
            format!("/friendrequests/accept/{friendrequest_id}"),
            None,
            None,
            sender,
        );

        self.accept_response = Some(receiver);
    }

    fn decline(&mut self, friendrequest_id: usize) {
        info!("Decline friendrequest fired for id: {friendrequest_id}");
        let (sender, receiver) =
            oneshot::channel::<Result<DeclineFriendrequestResponse, HttpError>>();

        HttpClientHandle::do_authenticated_request(
            Method::PATCH,
            format!("/friendrequests/decline/{friendrequest_id}"),
            None,
            None,
            sender,
        );

        self.decline_response = Some(receiver);
    }

    fn cancel(&mut self, friendrequest_id: usize) {
        info!("Cancel/delete friendrequest fired for id: {friendrequest_id}");
        let (sender, receiver) =
            oneshot::channel::<Result<DeleteFriendrequestResponse, HttpError>>();

        HttpClientHandle::do_authenticated_request(
            Method::DELETE,
            format!("/friendrequests/{friendrequest_id}"),
            None,
            None,
            sender,
        );

        self.cancel_response = Some(receiver);
    }

    fn handle_responses(&mut self) {
        HttpClientHandle::handle_response_data(
            &mut self.get_user_response,
            |err: HttpError| {
                error!("Get users request error: {:?}", err);
                self.error_message = Some(err.message);
            },
            |data: GetUserResponse| {
                info!("Get user was successfull");
                self.friendrequest_user = Some(data.result);
            },
        );

        HttpClientHandle::handle_response_data(
            &mut self.accept_response,
            |err: HttpError| {
                error!("Accept friendrequest request error: {:?}", err);
                self.error_message = Some(err.message);
            },
            |_data: AcceptFriendrequestResponse| {
                info!("Accept friendrequest request was successfull");
                self.friendrequests_handle.fetch();
                self.friends_handle.fetch();
            },
        );

        HttpClientHandle::handle_response_data(
            &mut self.decline_response,
            |err: HttpError| {
                error!("Decline friendrequest request error: {:?}", err);
                self.error_message = Some(err.message);
            },
            |_data: DeclineFriendrequestResponse| {
                info!("Decline friendrequest request was successfull");
                self.friendrequests_handle.fetch();
            },
        );

        HttpClientHandle::handle_response_data(
            &mut self.cancel_response,
            |err: HttpError| {
                error!("Delete friendrequest request error: {:?}", err);
                self.error_message = Some(err.message);
            },
            |_data: DeleteFriendrequestResponse| {
                info!("Delete friendrequest request was successfull");
                self.friendrequests_handle.fetch();
            },
        );
    }
}
