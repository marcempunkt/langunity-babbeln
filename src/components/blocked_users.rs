use crate::actors::{
    app_actor::use_app_handle,
    blocked_users_actor::{use_blocked_users_handle, BlockedUsersHandle},
};
use crate::components::blocked_user_item::BlockedUserItem;
use crate::core::models::User;
use crate::core::task::spawn_blocking;
use crate::core::utils::handle_oneshot_receiver;
use egui::{Context, Ui};
use log::info;
use tokio::sync::oneshot;

pub struct BlockedUsers {
    blocked_users_handle: BlockedUsersHandle,
    blocked_user_items: Vec<BlockedUserItem>,
    get_blocked_user_items: Option<oneshot::Receiver<Vec<BlockedUserItem>>>,
}

impl BlockedUsers {
    pub fn new() -> Self {
        Self {
            blocked_users_handle: use_blocked_users_handle(),
            blocked_user_items: Vec::new(),
            get_blocked_user_items: None,
        }
    }

    pub fn render(&mut self, ui: &mut Ui, ctx: &Context) {
        self.handle_update_blocked_user_items();

        if self.blocked_user_items.len() > 0 {
            egui::ScrollArea::vertical()
                .max_width(std::f32::INFINITY)
                .max_height(std::f32::INFINITY)
                .auto_shrink([false, false])
                .show_viewport(ui, |ui: &mut Ui, _| {
                    for blocked_user_item in &mut self.blocked_user_items {
                        blocked_user_item.render(ui, ctx);
                    }
                });
        } else {
            ui.label("no blocked users :)");
        }
    }

    fn handle_update_blocked_user_items(&mut self) {
        if self
            .blocked_users_handle
            .blocked_users
            .has_changed()
            .unwrap()
        {
            info!("Blocked_users changed");
            self.build_blocked_user_items();
        }

        handle_oneshot_receiver(
            &mut self.get_blocked_user_items,
            |data: Vec<BlockedUserItem>| self.blocked_user_items = data,
        );
    }

    fn build_blocked_user_items(&mut self) {
        info!("Rebuild blocked_user_items");
        let (sender, receiver) = oneshot::channel::<Vec<BlockedUserItem>>();
        let blocked_users = self.blocked_users_handle.get_blocked_users();

        spawn_blocking(|| {
            let blocked_user_items = blocked_users
                .into_iter()
                .map(|user: User| BlockedUserItem::new(user))
                .collect();
            let _ = sender.send(blocked_user_items);
            use_app_handle().request_repaint();
        });

        self.get_blocked_user_items = Some(receiver);
    }
}
