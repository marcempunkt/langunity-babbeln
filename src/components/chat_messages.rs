use crate::actors::{
    app_actor::{use_app_handle, AppHandle},
    messages_actor::{use_messages_handle, MessagesHandle},
};
use crate::components::message_item::MessageItem;
use crate::core::models::Message;
use crate::core::task::spawn_blocking;
use crate::core::utils::handle_oneshot_receiver;
use egui::{Context, Ui};
use log::info;
use tokio::sync::oneshot;

pub struct ChatMessages {
    app_handle: AppHandle,
    messages_handle: MessagesHandle,
    message_items: Vec<MessageItem>,
    get_message_items: Option<oneshot::Receiver<Vec<MessageItem>>>,
}

impl ChatMessages {
    pub fn new() -> Self {
        Self {
            app_handle: use_app_handle(),
            messages_handle: use_messages_handle(),
            message_items: Vec::new(),
            get_message_items: None,
        }
    }

    pub fn render(&mut self, ui: &mut Ui, ctx: &Context, chat_partner_username: &str) {
        self.handle_update_message_items();

        egui::ScrollArea::vertical()
            .max_width(std::f32::INFINITY)
            .max_height(std::f32::INFINITY)
            .auto_shrink([false, false])
            .stick_to_bottom(true)
            .show_viewport(ui, |ui: &mut Ui, _| {
                if ui.button("load more messages").clicked() {}

                for message_item in &mut self.message_items {
                    message_item.render(ui, ctx, chat_partner_username);
                }
            });
    }

    fn handle_update_message_items(&mut self) {
        if self.messages_handle.chats.has_changed().unwrap()
            || self.app_handle.show_chat.has_changed().unwrap()
        {
            info!("Messages changed");
            self.build_message_items();
        }

        handle_oneshot_receiver(&mut self.get_message_items, |data: Vec<MessageItem>| {
            info!("Build MessageItems finished");
            self.message_items = data
        });
    }

    fn build_message_items(&mut self) {
        if let Some(chat_partner_id) = self.app_handle.get_show_chat() {
            info!("Build MessageItems");
            let chat: Vec<Message> = self.messages_handle.get_chat(chat_partner_id);
            let (sender, receiver) = oneshot::channel::<Vec<MessageItem>>();

            spawn_blocking(|| {
                let message_items: Vec<MessageItem> = chat
                    .into_iter()
                    .rev()
                    .map(|message: Message| MessageItem::new(message))
                    .collect();

                let _ = sender.send(message_items);
                use_app_handle().request_repaint();
            });

            self.get_message_items = Some(receiver);
        }
    }
}
