use crate::core::task::spawn;
use eframe::egui::Context;
use log::info;
use std::sync::LazyLock;
use tokio::sync::{mpsc, watch};

pub static APP_HANDLE: LazyLock<AppHandle> = LazyLock::new(|| {
    info!("Created APP_HANDLE");
    let (app_actor, app_handle) = AppActor::new();
    AppActor::run(app_actor);
    app_handle
});

pub fn use_app_handle() -> AppHandle {
    APP_HANDLE.clone()
}

enum ActorMessage {
    SetCtx(Context),
    SetShowDashboard,
    SetShowFriends,
    SetShowTimeline,
    SetShowChat(usize),
}

pub struct AppActor {
    pub ctx: watch::Sender<Option<Context>>,
    pub show_dashboard: watch::Sender<bool>,
    pub show_friends: watch::Sender<bool>,
    pub show_timeline: watch::Sender<bool>,
    pub show_chat: watch::Sender<Option<usize>>,
    receiver: mpsc::UnboundedReceiver<ActorMessage>,
}

impl AppActor {
    pub fn new() -> (Self, AppHandle) {
        let (sender, receiver) = mpsc::unbounded_channel::<ActorMessage>();

        let (update_ctx, watcher_ctx) = watch::channel::<Option<Context>>(None);
        let (update_show_dashboard, watcher_show_dashboard) = watch::channel::<bool>(true);
        let (update_show_friends, watcher_show_friends) = watch::channel::<bool>(false);
        let (update_show_timeline, watcher_show_timeline) = watch::channel::<bool>(false);
        let (update_show_chat, watcher_show_chat) = watch::channel::<Option<usize>>(None);

        let app_actor = Self {
            ctx: update_ctx,
            show_dashboard: update_show_dashboard,
            show_friends: update_show_friends,
            show_timeline: update_show_timeline,
            show_chat: update_show_chat,
            receiver,
        };

        let app_handle = AppHandle::new(
            sender,
            watcher_ctx,
            watcher_show_dashboard,
            watcher_show_friends,
            watcher_show_timeline,
            watcher_show_chat,
        );

        (app_actor, app_handle)
    }

    pub fn run(app_actor: Self) {
        info!("Started AppActor event loop");
        spawn(app_actor.event_loop());
    }

    async fn event_loop(mut self) {
        while let Some(message) = self.receiver.recv().await {
            match message {
                ActorMessage::SetCtx(ctx) => {
                    // info!("AppActor ActorMessage::SetCtx");
                    let _ = self.ctx.send(Some(ctx));
                }
                ActorMessage::SetShowDashboard => {
                    info!("AppActor ActorMessage::SetShowDashboard");
                    let _ = self.show_dashboard.send(true);
                    let _ = self.show_friends.send(false);
                    let _ = self.show_timeline.send(false);
                    let _ = self.show_chat.send(None);
                }
                ActorMessage::SetShowFriends => {
                    info!("AppActor ActorMessage::SetShowFriends");
                    let _ = self.show_dashboard.send(false);
                    let _ = self.show_friends.send(true);
                    let _ = self.show_timeline.send(false);
                    let _ = self.show_chat.send(None);
                }
                ActorMessage::SetShowTimeline => {
                    info!("AppActor ActorMessage::SetShowTimeline");
                    let _ = self.show_dashboard.send(false);
                    let _ = self.show_friends.send(false);
                    let _ = self.show_timeline.send(true);
                    let _ = self.show_chat.send(None);
                }
                ActorMessage::SetShowChat(user_id) => {
                    info!("AppActor ActorMessage::SetShowChat");
                    let _ = self.show_dashboard.send(false);
                    let _ = self.show_friends.send(false);
                    let _ = self.show_timeline.send(false);
                    let _ = self.show_chat.send(Some(user_id));
                }
            }
        }
    }
}

#[derive(Clone)]
pub struct AppHandle {
    pub ctx: watch::Receiver<Option<Context>>,
    pub show_dashboard: watch::Receiver<bool>,
    pub show_friends: watch::Receiver<bool>,
    pub show_timeline: watch::Receiver<bool>,
    pub show_chat: watch::Receiver<Option<usize>>,
    sender: mpsc::UnboundedSender<ActorMessage>,
}

impl AppHandle {
    fn new(
        sender: mpsc::UnboundedSender<ActorMessage>,
        ctx: watch::Receiver<Option<Context>>,
        show_dashboard: watch::Receiver<bool>,
        show_friends: watch::Receiver<bool>,
        show_timeline: watch::Receiver<bool>,
        show_chat: watch::Receiver<Option<usize>>,
    ) -> Self {
        Self {
            sender,
            ctx,
            show_dashboard,
            show_friends,
            show_timeline,
            show_chat,
        }
    }

    pub fn get_ctx(&mut self) -> Option<Context> {
        self.ctx.borrow_and_update().clone()
    }

    pub fn set_ctx(&self, ctx: Context) {
        let _ = self.sender.send(ActorMessage::SetCtx(ctx));
    }

    pub fn get_show_dashboard(&mut self) -> bool {
        *self.show_dashboard.borrow_and_update()
    }

    pub fn set_show_dashboard(&self) {
        let _ = self.sender.send(ActorMessage::SetShowDashboard);
    }

    pub fn get_show_friends(&mut self) -> bool {
        *self.show_friends.borrow_and_update()
    }

    pub fn set_show_friends(&self) {
        let _ = self.sender.send(ActorMessage::SetShowFriends);
    }

    pub fn get_show_timeline(&mut self) -> bool {
        *self.show_timeline.borrow_and_update()
    }

    pub fn set_show_timeline(&self) {
        let _ = self.sender.send(ActorMessage::SetShowTimeline);
    }

    pub fn get_show_chat(&mut self) -> Option<usize> {
        *self.show_chat.borrow_and_update()
    }

    pub fn set_show_chat(&self, user_id: usize) {
        let _ = self.sender.send(ActorMessage::SetShowChat(user_id));
    }

    pub fn reset_state(&mut self) {
        info!("Resetted state");
        self.set_show_dashboard();
    }

    pub fn request_repaint(&mut self) {
        if let Some(ctx) = self.get_ctx() {
            ctx.request_repaint();
        }
    }
}
