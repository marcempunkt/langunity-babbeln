use crate::actors::{
    app_actor::use_app_handle, blocked_users_actor::use_blocked_users_handle,
    chat_partners_actor::use_chat_partners_handle, friendrequests_actor::use_friendrequests_handle,
    friends_actor::use_friends_handle, messages_actor::use_messages_handle,
    posts_actor::use_posts_handle, user_actor::use_user_handle,
};
use crate::core::responses::RefreshTokensResponse;
use crate::core::task::spawn;
use crate::core::utils::wait_for;
use async_recursion::async_recursion;
use log::{error, info};
use reqwest::{Client, ClientBuilder, Method, RequestBuilder, StatusCode};
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use serde_json::json;
use std::sync::LazyLock;
use std::time::Duration;
use tokio::sync::{mpsc, oneshot, watch};

pub static HTTP_CLIENT_HANDLE: LazyLock<HttpClientHandle> = LazyLock::new(|| {
    info!("Created HTTP_CLIENT_HANDLE");
    let (http_client_actor, http_client_handle) = HttpClientActor::new();
    HttpClientActor::run(http_client_actor);
    http_client_handle
});

pub fn use_http_client_handle() -> HttpClientHandle {
    HTTP_CLIENT_HANDLE.clone()
}

#[derive(Serialize, Deserialize, Debug)]
pub struct HttpError {
    pub status: u16,
    pub message: String,
}

enum ActorMessage {
    SetAccessToken(Option<String>),
    SetRefreshToken(Option<String>),
}

pub struct HttpClientActor {
    pub access_token: watch::Sender<Option<String>>,
    pub refresh_token: watch::Sender<Option<String>>,
    receiver: mpsc::UnboundedReceiver<ActorMessage>,
}

impl HttpClientActor {
    pub fn new() -> (Self, HttpClientHandle) {
        let (sender, receiver) = mpsc::unbounded_channel::<ActorMessage>();

        let (update_access_token, watcher_access_token) = watch::channel::<Option<String>>(None);
        let (update_refresh_token, watcher_refresh_token) = watch::channel::<Option<String>>(None);

        let http_client_actor = Self {
            access_token: update_access_token,
            refresh_token: update_refresh_token,
            receiver,
        };

        let http_client_handle =
            HttpClientHandle::new(watcher_access_token, watcher_refresh_token, sender);

        (http_client_actor, http_client_handle)
    }

    pub fn run(http_client_actor: Self) {
        info!("Started HttpClientActor event loop");
        spawn(http_client_actor.event_loop());
    }

    async fn event_loop(mut self) {
        while let Some(message) = self.receiver.recv().await {
            match message {
                ActorMessage::SetAccessToken(token) => {
                    info!("HttpClientActor ActorMessage::SetAccessToken");
                    let _ = self.access_token.send(token);
                }
                ActorMessage::SetRefreshToken(token) => {
                    info!("HttpClientActor ActorMessage::SetRefreshToken");
                    let _ = self.refresh_token.send(token);
                }
            }
        }
    }
}

#[derive(Clone)]
pub struct HttpClientHandle {
    pub access_token: watch::Receiver<Option<String>>,
    pub refresh_token: watch::Receiver<Option<String>>,
    pub base_url: String,
    sender: mpsc::UnboundedSender<ActorMessage>,
}

impl HttpClientHandle {
    fn new(
        access_token: watch::Receiver<Option<String>>,
        refresh_token: watch::Receiver<Option<String>>,
        sender: mpsc::UnboundedSender<ActorMessage>,
    ) -> Self {
        let base_url: String = "http://localhost:4000".to_string();
        Self {
            access_token,
            refresh_token,
            base_url,
            sender,
        }
    }

    pub fn get_access_token(&mut self) -> Option<String> {
        self.access_token.borrow_and_update().clone()
    }

    pub fn set_access_token(&self, token: Option<String>) {
        info!("HttpClientHandle set_access_token");
        #[cfg(target_arch = "wasm32")]
        if let Some(token) = token.clone() {
            use web_sys::wasm_bindgen::JsCast;
            let bearer_cookie: String = format!("Bearer={token}");
            let document = gloo_utils::document().unchecked_into::<web_sys::HtmlDocument>();
            document.set_cookie(&bearer_cookie).unwrap();
        }

        let _ = self.sender.send(ActorMessage::SetAccessToken(token));
    }

    pub async fn get_access_token_async(&mut self) -> String {
        let _ = self.access_token.wait_for(|t| t.is_some()).await;
        self.access_token.borrow_and_update().clone().unwrap()
    }

    pub fn get_refresh_token(&mut self) -> Option<String> {
        self.refresh_token.borrow_and_update().clone()
    }

    pub async fn get_refresh_token_async(&mut self) -> String {
        let _ = self.refresh_token.wait_for(|t| t.is_some()).await;
        self.refresh_token.borrow_and_update().clone().unwrap()
    }

    pub fn set_refresh_token(&self, token: Option<String>) {
        info!("HttpClientHandle set_refresh_token");
        let _ = self.sender.send(ActorMessage::SetRefreshToken(token));
    }

    pub fn reset_state(&self) {
        info!("Resetted state");
        self.set_access_token(None);
        self.set_refresh_token(None);
    }

    // Waits till access and refresh token has been set to create a request
    // that has the access token as a bearer token in the headers
    // Works the same as the `create_request` method
    pub async fn create_authenticated_request(
        &mut self,
        method: Method,
        route: impl Into<String>,
    ) -> RequestBuilder {
        let request: RequestBuilder = Self::create_request(method, route);
        let _ = self.access_token.wait_for(|t| t.is_some()).await;
        let _ = self.refresh_token.wait_for(|t| t.is_some()).await;
        let bearer: String = self.get_access_token().unwrap();
        request.bearer_auth(&bearer)
    }

    /// Helper function to create a simple request.
    /// Takes in a method and route to create a new request builder from the reqwest crate
    pub fn create_request(method: Method, route: impl Into<String>) -> RequestBuilder {
        let route: String = route.into();
        let client: Client = ClientBuilder::new().build().unwrap();

        match method {
            Method::GET => client.get(route),
            Method::POST => client.post(route),
            Method::PATCH => client.patch(route),
            Method::DELETE => client.delete(route),
            _ => panic!("Unsupported HTTP Method: {}", method),
        }
    }

    /// Converts a reqwest Result response into the data of the response or into an HttpError
    /// This fn is exactly a duplicate of the other `handle_response` with `target_arch="wasm32"`
    /// TODO if someone knows how to apply a function macro conditionally depending on the
    /// architecture without unnecessary copy&past-ing please PR/MR please!!!
    #[cfg(not(target_arch = "wasm32"))]
    #[async_recursion]
    pub async fn handle_response<TResponse: DeserializeOwned>(
        result: Result<reqwest::Response, reqwest::Error>,
        cloned_request: RequestBuilder,
    ) -> Result<TResponse, HttpError> {
        match result {
            Ok(response) => match response.status() {
                StatusCode::OK | StatusCode::CREATED => {
                    Ok(response.json::<TResponse>().await.unwrap())
                }
                _ => Err(response.json::<HttpError>().await.unwrap()),
            },
            Err(e) => {
                // Will error (maybe) only on network error
                // If so recursively redo the request
                // until it can be converted into a Response/HttpError
                wait_for(Duration::from_secs(5)).await;
                let request_cloned = cloned_request.try_clone().unwrap();
                let response = cloned_request.send().await;
                HttpClientHandle::handle_response(response, request_cloned).await
            }
        }
    }

    /// Converts a reqwest Result response into the data of the response or into an HttpError
    /// This fn is exactly a duplicate of the other `handle_response` with `target_arch="wasm32"`
    /// TODO if someone knows how to apply a function macro conditionally depending on the
    /// architecture without unnecessary copy&past-ing please PR/MR please!!!
    #[cfg(target_arch = "wasm32")]
    #[async_recursion(?Send)]
    pub async fn handle_response<TResponse: DeserializeOwned>(
        result: Result<reqwest::Response, reqwest::Error>,
        cloned_request: RequestBuilder,
    ) -> Result<TResponse, HttpError> {
        match result {
            Ok(response) => match response.status() {
                StatusCode::OK | StatusCode::CREATED => {
                    Ok(response.json::<TResponse>().await.unwrap())
                }
                _ => Err(response.json::<HttpError>().await.unwrap()),
            },
            Err(e) => {
                // Will error (maybe) only on network error
                // If so recursively redo the request
                // until it can be converted into a Response/HttpError
                wait_for(Duration::from_secs(5)).await;
                let request_cloned = cloned_request.try_clone().unwrap();
                let response = cloned_request.send().await;
                HttpClientHandle::handle_response(response, request_cloned).await
            }
        }
    }

    pub fn handle_response_data<TResponse>(
        response: &mut Option<oneshot::Receiver<Result<TResponse, HttpError>>>,
        on_error: impl FnOnce(HttpError),
        on_success: impl FnOnce(TResponse),
    ) {
        if let Some(receiver) = response {
            match receiver.try_recv() {
                Ok(response) => {
                    match response {
                        Ok(data) => {
                            on_success(data);
                        }
                        Err(err) => {
                            on_error(err);
                        }
                    };
                    use_app_handle().request_repaint();
                }
                Err(oneshot::error::TryRecvError::Closed) => *response = None,
                Err(oneshot::error::TryRecvError::Empty) => (),
            }
        }
    }

    pub fn do_authenticated_request<TResponse>(
        method: Method,
        pathname: impl Into<String>,
        body: Option<serde_json::Value>,
        query: Option<Vec<(String, String)>>,
        sender: oneshot::Sender<Result<TResponse, HttpError>>,
    ) where
        TResponse: DeserializeOwned + Send + 'static,
    {
        let pathname: String = pathname.into();
        let mut http_client_handle = use_http_client_handle();
        let base_url: &str = &http_client_handle.base_url;
        let route: String = match pathname.contains(base_url) {
            true => pathname,
            false => format!("{base_url}{pathname}"),
        };

        info!("Doing authenticated request to {}", &route);

        fn maybe_add_query_and_body(
            mut request: RequestBuilder,
            query: Option<Vec<(String, String)>>,
            body: Option<serde_json::Value>,
        ) -> RequestBuilder {
            if let Some(ref body) = body {
                request = request.json(body);
            }

            if let Some(ref query) = query {
                request = request.query(query);
            }

            request
        }

        spawn(async move {
            let mut request: RequestBuilder = http_client_handle
                .create_authenticated_request(method.clone(), route.clone())
                .await;

            request = maybe_add_query_and_body(request, query.clone(), body.clone());

            let request_cloned = (request.try_clone().unwrap(), request.try_clone().unwrap());
            let response = request.send().await;
            let response: Result<TResponse, HttpError> =
                HttpClientHandle::handle_response(response, request_cloned.0).await;

            if let Err(http_error) = response {
                if http_error.status == StatusCode::UNAUTHORIZED {
                    if let Some(refresh_token) = http_client_handle.get_refresh_token() {
                        info!("Refresh authentication tokens");

                        let (refresh_sender, refresh_receiver) =
                            oneshot::channel::<Result<RefreshTokensResponse, HttpError>>();
                        HttpClientHandle::do_request(
                            Method::POST,
                            "/token/refresh",
                            Some(json!({
                                "refresh": refresh_token,
                            })),
                            None,
                            refresh_sender,
                        );
                        let refresh_request: Result<RefreshTokensResponse, HttpError> =
                            refresh_receiver.await.unwrap();

                        let last_response: Result<TResponse, HttpError> = match refresh_request {
                            Ok(refresh_response) => {
                                info!("Refresh authentication tokens was successfull");
                                // Wait till the access & refresh token state has been updated
                                let _ = http_client_handle.access_token.borrow_and_update();
                                let _ = http_client_handle.refresh_token.borrow_and_update();

                                http_client_handle.set_access_token(Some(refresh_response.access));
                                http_client_handle
                                    .set_refresh_token(Some(refresh_response.refresh));

                                http_client_handle.access_token.changed().await.unwrap();
                                http_client_handle.refresh_token.changed().await.unwrap();

                                let mut redo_request: RequestBuilder = http_client_handle
                                    .create_authenticated_request(method, &route)
                                    .await;
                                redo_request = maybe_add_query_and_body(redo_request, query, body);

                                let redo_request_cloned: RequestBuilder =
                                    redo_request.try_clone().unwrap();

                                let redo_response = redo_request.send().await;

                                HttpClientHandle::handle_response(
                                    redo_response,
                                    redo_request_cloned,
                                )
                                .await
                            }
                            Err(http_error) => {
                                error!("Refresh authentication tokens failed");
                                HttpClientHandle::reset_all();
                                Err(http_error)
                            }
                        };
                        let _ = sender.send(last_response);
                    }
                }
            } else {
                let _ = sender.send(response);
            }

            info!("Finished authenticated request to: {}", &route);
            use_app_handle().request_repaint();
        });
    }

    pub fn do_request<TResponse>(
        method: Method,
        pathname: impl Into<String>,
        body: Option<serde_json::Value>,
        query: Option<Vec<(String, String)>>,
        sender: oneshot::Sender<Result<TResponse, HttpError>>,
    ) where
        TResponse: DeserializeOwned + Send + 'static,
    {
        let pathname: String = pathname.into();
        let mut http_client_handle = use_http_client_handle();
        let base_url: &str = &http_client_handle.base_url;
        let route: String = format!("{base_url}{pathname}");

        info!("Doing request to {}", &route);

        let mut request = HttpClientHandle::create_request(method, &route);

        if let Some(body) = body {
            request = request.json(&body);
        }

        if let Some(query) = query {
            request = request.json(&query);
        }

        let request_cloned = request.try_clone().unwrap();

        spawn(async move {
            let response = request.send().await;
            let response: Result<TResponse, HttpError> =
                HttpClientHandle::handle_response(response, request_cloned).await;

            let _ = sender.send(response);
            info!("Finished request to: {}", &route);
            use_app_handle().request_repaint();
        });
    }

    pub fn fetch_all() {
        info!("Fetch all actors state");
        spawn(async {
            // First try to fetch "me" and
            // refresh authentication tokens if necessary
            let mut user_handle = use_user_handle();
            let _ = user_handle.me.borrow_and_update();
            user_handle.fetch();
            user_handle.me.changed().await.unwrap();

            // Then fetch the rest
            use_blocked_users_handle().fetch();
            use_chat_partners_handle().fetch();
            use_friendrequests_handle().fetch();
            use_friends_handle().fetch();
            use_posts_handle().fetch();
            // use_messages_handle().fetch();

            use_app_handle().request_repaint();
        });
    }

    pub fn reset_all() {
        info!("Reset all actors state");
        use_app_handle().reset_state();
        use_blocked_users_handle().reset_state();
        use_chat_partners_handle().reset_state();
        use_friendrequests_handle().reset_state();
        use_friends_handle().reset_state();
        use_http_client_handle().reset_state();
        use_messages_handle().reset_state();
        use_posts_handle().reset_state();
        use_user_handle().reset_state();

        use_app_handle().request_repaint();
    }
}
