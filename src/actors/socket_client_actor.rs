use crate::actors::http_client_actor::use_http_client_handle;
use crate::core::task::spawn;
use crate::lib::socket_event::SocketEvent;
use eframe::egui::Context;
use log::info;
use std::sync::LazyLock;
use tokio::sync::{mpsc, watch};

#[cfg(not(target_arch = "wasm32"))]
pub use crate::actors::socket_client_actor_native::{ActorMessage, SocketClientActor};
#[cfg(target_arch = "wasm32")]
pub use crate::actors::socket_client_actor_wasm::{ActorMessage, SocketClientActor};

pub static SOCKET_CLIENT_HANDLE: LazyLock<SocketClientHandle> = LazyLock::new(|| {
    info!("Created SOCKET_CLIENT_HANDLE");
    let (socket_client_actor, socket_client_handle) = SocketClientActor::new();
    SocketClientActor::run(socket_client_actor);
    socket_client_handle
});

pub fn use_socket_client_handle() -> SocketClientHandle {
    SOCKET_CLIENT_HANDLE.clone()
}

#[derive(Clone)]
pub struct SocketClientHandle {
    pub sender: mpsc::UnboundedSender<ActorMessage>,
}

impl SocketClientHandle {
    pub fn new(sender: mpsc::UnboundedSender<ActorMessage>) -> Self {
        Self { sender }
    }

    pub fn emit(&self, socket_event: SocketEvent) {
        info!("Emit socket event: {:?}", socket_event);
        let _ = self
            .sender
            .send(ActorMessage::EmitSocketEvent(socket_event));
    }

    pub fn handle_received_socket_event(&self, socket_event: impl Into<String>) {
        if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&socket_event.into()) {
            info!("Handle received socket event: {:?}", &socket_event);
            let _ = self
                .sender
                .send(ActorMessage::HandleReceivedSocketEvent(socket_event));
        }
    }

    pub fn reconnect(&self) {
        info!("Reconnect to socket server");
        let _ = self.sender.send(ActorMessage::Reconnect);
    }
}
