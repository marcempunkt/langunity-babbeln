use crate::actors::{
    app_actor::use_app_handle,
    http_client_actor::{use_http_client_handle, HttpClientHandle, HttpError},
};
use crate::core::models::Friendrequest;
use crate::core::responses::GetFriendrequestsResponse;
use crate::core::task::spawn;
use log::{error, info};
use reqwest::Method;
use std::sync::LazyLock;
use tokio::sync::{mpsc, oneshot, watch};

pub static FRIENDREQUESTS_HANDLE: LazyLock<FriendrequestsHandle> = LazyLock::new(|| {
    info!("Created FRIENDREQUESTS_HANDLE");
    let (friendrequests_actor, friendrequests_handle) = FriendrequestsActor::new();
    FriendrequestsActor::run(friendrequests_actor);
    friendrequests_handle
});

pub fn use_friendrequests_handle() -> FriendrequestsHandle {
    FRIENDREQUESTS_HANDLE.clone()
}

enum ActorMessage {
    Fetch,
    SetFriendrequests(Vec<Friendrequest>),
}

pub struct FriendrequestsActor {
    pub friendrequests: watch::Sender<Vec<Friendrequest>>,
    receiver: mpsc::UnboundedReceiver<ActorMessage>,
}

impl FriendrequestsActor {
    pub fn new() -> (Self, FriendrequestsHandle) {
        let (sender, receiver) = mpsc::unbounded_channel::<ActorMessage>();

        let (update_friendrequests, watcher_friendrequests) =
            watch::channel::<Vec<Friendrequest>>(Vec::new());

        let friendrequests_actor = Self {
            friendrequests: update_friendrequests,
            receiver,
        };

        let friendrequests_handle = FriendrequestsHandle::new(watcher_friendrequests, sender);

        (friendrequests_actor, friendrequests_handle)
    }

    pub fn run(friendrequests_actor: Self) {
        info!("Started friendrequests actor event loop");
        spawn(friendrequests_actor.event_loop());
    }

    async fn event_loop(mut self) {
        while let Some(message) = self.receiver.recv().await {
            match message {
                ActorMessage::Fetch => {
                    info!("FriendrequestsActor ActorMessage::Fetch");
                    FriendrequestsActor::fetch();
                }
                ActorMessage::SetFriendrequests(friendrequests) => {
                    info!("FriendrequestsActor ActorMessage::SetFriendrequests");
                    let _ = self.friendrequests.send(friendrequests);
                }
            }
        }
    }

    fn fetch() {
        spawn(async move {
            let (sender, receiver) =
                oneshot::channel::<Result<GetFriendrequestsResponse, HttpError>>();
            HttpClientHandle::do_authenticated_request(
                Method::GET,
                "/friendrequests/",
                None,
                None,
                sender,
            );

            match receiver.await.unwrap() {
                Ok(response) => {
                    let friendrequests_handle: FriendrequestsHandle = use_friendrequests_handle();
                    friendrequests_handle.set_friendrequests(response.result);
                }
                Err(http_error) => error!("{:?}", http_error),
            }
        });
    }
}

#[derive(Clone)]
pub struct FriendrequestsHandle {
    pub friendrequests: watch::Receiver<Vec<Friendrequest>>,
    sender: mpsc::UnboundedSender<ActorMessage>,
}

impl FriendrequestsHandle {
    fn new(
        friendrequests: watch::Receiver<Vec<Friendrequest>>,
        sender: mpsc::UnboundedSender<ActorMessage>,
    ) -> Self {
        Self {
            friendrequests,
            sender,
        }
    }

    pub fn fetch(&self) {
        let _ = self.sender.send(ActorMessage::Fetch);
    }

    pub fn reset_state(&self) {
        info!("Resetted state");
        self.set_friendrequests(Vec::new());
    }

    pub fn get_friendrequests(&mut self) -> Vec<Friendrequest> {
        self.friendrequests.borrow_and_update().clone()
    }

    pub fn set_friendrequests(&self, friendrequests: Vec<Friendrequest>) {
        let _ = self
            .sender
            .send(ActorMessage::SetFriendrequests(friendrequests));
    }
}
