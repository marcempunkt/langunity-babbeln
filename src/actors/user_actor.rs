use crate::actors::{
    app_actor::use_app_handle,
    blocked_users_actor::use_blocked_users_handle,
    friends_actor::use_friends_handle,
    http_client_actor::{use_http_client_handle, HttpClientHandle, HttpError},
};
use crate::core::models::User;
use crate::core::responses::GetUserResponse;
use crate::core::task::spawn;
use log::{error, info};
use regex::Regex;
use reqwest::Method;
use std::str::FromStr;
use std::sync::LazyLock;
use tokio::sync::{mpsc, oneshot, watch};

pub static USER_HANDLE: LazyLock<UserHandle> = LazyLock::new(|| {
    info!("Created USER_HANDLE");
    let (user_actor, user_handle) = UserActor::new();
    UserActor::run(user_actor);
    user_handle
});

pub fn use_user_handle() -> UserHandle {
    USER_HANDLE.clone()
}

enum ActorMessage {
    Fetch,
    SetMe(Option<User>),
}

pub struct UserActor {
    pub me: watch::Sender<Option<User>>,
    receiver: mpsc::UnboundedReceiver<ActorMessage>,
}

impl UserActor {
    pub fn new() -> (Self, UserHandle) {
        let (sender, receiver) = mpsc::unbounded_channel::<ActorMessage>();

        let (update_me, watcher_me) = watch::channel::<Option<User>>(None);

        let user_actor = Self {
            me: update_me,
            receiver,
        };

        let user_handle = UserHandle::new(watcher_me, sender);

        (user_actor, user_handle)
    }

    pub fn run(user_actor: Self) {
        info!("Started user actor event loop");
        spawn(user_actor.event_loop());
    }

    async fn event_loop(mut self) {
        while let Some(message) = self.receiver.recv().await {
            match message {
                ActorMessage::Fetch => {
                    info!("UserActor ActorMessage::Fetch");
                    UserActor::fetch();
                }
                ActorMessage::SetMe(me) => {
                    info!("UserActor ActorMessage::SetMe");
                    let _ = self.me.send(me);
                }
            }
        }
    }

    fn fetch() {
        info!("Fetch me");
        spawn(async move {
            let (sender, receiver) = oneshot::channel::<Result<GetUserResponse, HttpError>>();
            HttpClientHandle::do_authenticated_request(
                Method::GET,
                "/users/me",
                None,
                None,
                sender,
            );

            match receiver.await.unwrap() {
                Ok(response) => {
                    info!("Fetch me was successfull");
                    let user_handle: UserHandle = use_user_handle();
                    user_handle.set_me(Some(response.result));
                }
                Err(http_error) => error!("Fetch me error: {:?}", http_error),
            }
        });
    }
}

#[derive(Clone)]
pub struct UserHandle {
    pub me: watch::Receiver<Option<User>>,
    sender: mpsc::UnboundedSender<ActorMessage>,
}

impl UserHandle {
    fn new(me: watch::Receiver<Option<User>>, sender: mpsc::UnboundedSender<ActorMessage>) -> Self {
        Self { me, sender }
    }

    pub fn fetch(&self) {
        let _ = self.sender.send(ActorMessage::Fetch);
    }

    pub fn reset_state(&self) {
        info!("Resetted state");
        self.set_me(None);
    }

    pub fn get_me(&mut self) -> Option<User> {
        self.me.borrow_and_update().clone()
    }

    pub fn set_me(&self, me: Option<User>) {
        let _ = self.sender.send(ActorMessage::SetMe(me));
    }

    pub fn get_user_by_id(user_id: usize) -> Option<User> {
        let mut user_handle = use_user_handle();
        let mut friends_handle = use_friends_handle();
        let friends: Vec<User> = friends_handle.get_friends();

        if let Some(me) = user_handle.get_me() {
            if me.id == user_id {
                return Some(me);
            }
        }

        for user in friends {
            if user.id == user_id {
                return Some(user);
            }
        }

        None
    }

    pub fn get_user_by_url(url: impl Into<String>) -> Option<User> {
        let url: String = url.into();
        let regex_is_user_url = Regex::new(r"^https?:\/\/[\w,\d,.]*(:\d+)?\/users\/\d+$").unwrap();
        let regex_get_user_id = Regex::new(r"users\/\d+$").unwrap();

        if !regex_is_user_url.is_match(&url) {
            return None;
        }

        if let Some(mat) = regex_get_user_id.find(&url) {
            let mat: String = String::from(mat.as_str());
            let mat: Vec<&str> = mat.rsplit("/").collect();

            if let Ok(user_id) = usize::from_str(mat[0]) {
                return UserHandle::get_user_by_id(user_id);
            }
        }

        None
    }

    pub fn display_username(user: &User) -> String {
        if let Some(me) = use_user_handle().get_me() {
            let is_me: bool = user.id == me.id;
            if is_me {
                return user.username.clone();
            }
        }

        let is_blocked: bool = use_blocked_users_handle().is_blocked(user);
        if is_blocked {
            return "blocked user".to_string();
        }

        let is_friend: bool = use_friends_handle().is_friend(user);
        if is_friend {
            return user.username.clone();
        }

        "unknown".to_string()
    }

    pub fn display_username_by_id(user_id: usize) -> String {
        let user: Option<User> = UserHandle::get_user_by_id(user_id);

        if let Some(user) = user {
            return UserHandle::display_username(&user);
        }

        "unknown".to_string()
    }

    pub fn display_username_by_url(url: impl Into<String>) -> String {
        let url: String = url.into();
        let user: Option<User> = UserHandle::get_user_by_url(url);

        if let Some(user) = user {
            return UserHandle::display_username(&user);
        }

        "unknown".to_string()
    }
}
