use crate::actors::{
    app_actor::use_app_handle,
    http_client_actor::{use_http_client_handle, HttpClientHandle, HttpError},
};
use crate::core::models::User;
use crate::core::responses::{BlockUserResponse, GetBlockedUsersResponse, UnblockUserResponse};
use crate::core::task::spawn;
use log::{error, info};
use reqwest::Method;
use serde_json::json;
use std::sync::LazyLock;
use tokio::sync::{mpsc, oneshot, watch};

pub static BLOCKED_USERS_HANDLE: LazyLock<BlockedUsersHandle> = LazyLock::new(|| {
    info!("Created BLOCKED_USER_HANDLE");
    let (blocked_users_actor, blocked_users_handle) = BlockedUsersActor::new();
    BlockedUsersActor::run(blocked_users_actor);
    blocked_users_handle
});

pub fn use_blocked_users_handle() -> BlockedUsersHandle {
    BLOCKED_USERS_HANDLE.clone()
}

enum ActorMessage {
    Fetch,
    SetBlockedUsers(Vec<User>),
}

pub struct BlockedUsersActor {
    pub blocked_users: watch::Sender<Vec<User>>,
    receiver: mpsc::UnboundedReceiver<ActorMessage>,
}

impl BlockedUsersActor {
    pub fn new() -> (Self, BlockedUsersHandle) {
        let (sender, receiver) = mpsc::unbounded_channel::<ActorMessage>();

        let (update_blocked_users, watcher_blocked_users) = watch::channel::<Vec<User>>(Vec::new());

        let blocked_users_actor = Self {
            blocked_users: update_blocked_users,
            receiver,
        };

        let blocked_users_handle = BlockedUsersHandle::new(watcher_blocked_users, sender);

        (blocked_users_actor, blocked_users_handle)
    }

    pub fn run(blocked_users_actor: Self) {
        info!("Started blocked user actor event loop");
        spawn(blocked_users_actor.event_loop());
    }

    async fn event_loop(mut self) {
        while let Some(message) = self.receiver.recv().await {
            match message {
                ActorMessage::Fetch => {
                    info!("BlockedUserActor ActorMessage::Fetch");
                    BlockedUsersActor::fetch();
                }
                ActorMessage::SetBlockedUsers(blocked_users) => {
                    info!("BlockedUserActor ActorMessage::SetBlockedUsers");
                    let _ = self.blocked_users.send(blocked_users);
                }
            }
        }
    }

    fn fetch() {
        info!("Fetch blocked users");
        spawn(async move {
            let (sender, receiver) =
                oneshot::channel::<Result<GetBlockedUsersResponse, HttpError>>();
            HttpClientHandle::do_authenticated_request(
                Method::GET,
                "/users/blocked-users/",
                None,
                None,
                sender,
            );

            match receiver.await.unwrap() {
                Ok(response) => {
                    info!("Fetch blocked users was successfull");
                    let blocked_users_handle: BlockedUsersHandle = use_blocked_users_handle();
                    blocked_users_handle.set_blocked_users(response.result);
                }
                Err(http_error) => error!("Fetch blocked users error: {:?}", http_error),
            }
        });
    }
}

#[derive(Clone)]
pub struct BlockedUsersHandle {
    pub blocked_users: watch::Receiver<Vec<User>>,
    sender: mpsc::UnboundedSender<ActorMessage>,
}

impl BlockedUsersHandle {
    fn new(
        blocked_users: watch::Receiver<Vec<User>>,
        sender: mpsc::UnboundedSender<ActorMessage>,
    ) -> Self {
        Self {
            blocked_users,
            sender,
        }
    }

    pub fn fetch(&self) {
        let _ = self.sender.send(ActorMessage::Fetch);
    }

    pub fn reset_state(&self) {
        info!("Resetted state");
        self.set_blocked_users(Vec::new());
    }

    pub fn get_blocked_users(&mut self) -> Vec<User> {
        self.blocked_users.borrow_and_update().clone()
    }

    pub fn set_blocked_users(&self, blocked_users: Vec<User>) {
        let _ = self
            .sender
            .send(ActorMessage::SetBlockedUsers(blocked_users));
    }

    pub fn is_blocked(&mut self, user: &User) -> bool {
        let blocked_user_ids: Vec<usize> = self
            .get_blocked_users()
            .into_iter()
            .map(|user: User| user.id)
            .collect();
        let is_blocked: bool = blocked_user_ids.contains(&user.id);
        is_blocked
    }

    pub fn block_user(
        user_id: usize,
        sender: oneshot::Sender<Result<BlockUserResponse, HttpError>>,
    ) {
        HttpClientHandle::do_authenticated_request(
            Method::POST,
            "/users/blocked-users/",
            Some(json!({
                "user_id": user_id,
            })),
            None,
            sender,
        );
    }

    pub fn unblock_user(
        user_id: usize,
        sender: oneshot::Sender<Result<UnblockUserResponse, HttpError>>,
    ) {
        HttpClientHandle::do_authenticated_request(
            Method::DELETE,
            format!("/users/blocked-users/{user_id}"),
            None,
            None,
            sender,
        );
    }
}
