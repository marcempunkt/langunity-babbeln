use crate::actors::{
    app_actor::use_app_handle,
    http_client_actor::{use_http_client_handle, HttpClientHandle, HttpError},
};
use crate::core::models::Message;
use crate::core::responses::GetChatResponse;
use crate::core::task::spawn;
use log::info;
use reqwest::Method;
use std::collections::HashMap;
use std::sync::LazyLock;
use tokio::sync::{mpsc, oneshot, watch};

pub static MESSAGES_HANDLE: LazyLock<MessagesHandle> = LazyLock::new(|| {
    info!("Created MESSAGES_HANDLE");
    let (messages_actor, messages_handle) = MessagesActor::new();
    MessagesActor::run(messages_actor);
    messages_handle
});

pub fn use_messages_handle() -> MessagesHandle {
    MESSAGES_HANDLE.clone()
}

enum ActorMessage {
    SetChats(HashMap<usize, Vec<Message>>),
}

pub struct MessagesActor {
    pub chats: watch::Sender<HashMap<usize, Vec<Message>>>,
    receiver: mpsc::UnboundedReceiver<ActorMessage>,
}

impl MessagesActor {
    pub fn new() -> (Self, MessagesHandle) {
        let (sender, receiver) = mpsc::unbounded_channel::<ActorMessage>();

        let (update_chats, watcher_chats) =
            watch::channel::<HashMap<usize, Vec<Message>>>(HashMap::new());

        let messages_actor = Self {
            chats: update_chats,
            receiver,
        };

        let messages_handle = MessagesHandle::new(watcher_chats, sender);

        (messages_actor, messages_handle)
    }

    pub fn run(messages_actor: Self) {
        info!("Started messages actor event loop");
        spawn(messages_actor.event_loop());
    }

    async fn event_loop(mut self) {
        while let Some(message) = self.receiver.recv().await {
            match message {
                ActorMessage::SetChats(chats) => {
                    info!("MessageActor ActorMessage::SetChats");
                    let _ = self.chats.send(chats);
                }
            }
        }
    }

    async fn add_messages(user_id: usize, messages: Vec<Message>) {
        todo!()
    }

    async fn remove_message(user_id: usize, message_id: usize) {
        todo!()
    }

    async fn update_message(user_id: usize, message_id: usize) {
        todo!()
    }
}

#[derive(Clone)]
pub struct MessagesHandle {
    pub chats: watch::Receiver<HashMap<usize, Vec<Message>>>,
    sender: mpsc::UnboundedSender<ActorMessage>,
}

impl MessagesHandle {
    fn new(
        chats: watch::Receiver<HashMap<usize, Vec<Message>>>,
        sender: mpsc::UnboundedSender<ActorMessage>,
    ) -> Self {
        Self { chats, sender }
    }

    pub fn reset_state(&self) {
        info!("Resetted state");
        self.set_chats(HashMap::new());
    }

    pub fn get_chats(&mut self) -> HashMap<usize, Vec<Message>> {
        self.chats.borrow_and_update().clone()
    }

    pub fn set_chats(&self, chats: HashMap<usize, Vec<Message>>) {
        let _ = self.sender.send(ActorMessage::SetChats(chats));
    }

    pub fn get_chat(&mut self, user_id: usize) -> Vec<Message> {
        let chats: HashMap<usize, Vec<Message>> = self.get_chats();
        match chats.get(&user_id) {
            Some(chats) => chats.clone(),
            None => Vec::new(),
        }
    }

    pub fn fetch_chat(&self, user_id: usize) {
        spawn(async move {
            async fn fetch_chat(user_id: usize, page: usize) -> Result<GetChatResponse, HttpError> {
                let (sender, receiver) = oneshot::channel::<Result<GetChatResponse, HttpError>>();
                HttpClientHandle::do_authenticated_request(
                    Method::GET,
                    format!("/messages/chat/{user_id}"),
                    None,
                    Some(vec![
                        ("page_size".to_string(), "50".to_string()),
                        ("page".to_string(), page.to_string()),
                    ]),
                    sender,
                );

                receiver.await.unwrap()
            }

            let mut messages_handle = use_messages_handle();
            let mut chats: HashMap<usize, Vec<Message>> = messages_handle.get_chats();

            let mut chat: Vec<Message> = Vec::new();
            let mut page: usize = 1;

            while let Ok(mut chat_response) = fetch_chat(user_id, page).await {
                page += 1;

                chat.append(&mut chat_response.result);

                if let None = chat_response.next {
                    break;
                }
            }

            chats.insert(user_id, chat);
            messages_handle.set_chats(chats);

            use_app_handle().request_repaint();
        });
    }
}
