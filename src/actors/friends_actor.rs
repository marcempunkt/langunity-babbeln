use crate::actors::{
    app_actor::use_app_handle,
    blocked_users_actor::use_blocked_users_handle,
    http_client_actor::{use_http_client_handle, HttpClientHandle, HttpError},
};
use crate::core::models::User;
use crate::core::responses::{GetFriendsResponse, RemoveFriendResponse};
use crate::core::task::spawn;
use log::{error, info};
use reqwest::Method;
use std::sync::LazyLock;
use tokio::sync::{mpsc, oneshot, watch};

pub static FRIENDS_HANDLE: LazyLock<FriendsHandle> = LazyLock::new(|| {
    info!("Created FRIENDS_HANDLE");
    let (friends_actor, friends_handle) = FriendsActor::new();
    FriendsActor::run(friends_actor);
    friends_handle
});

pub fn use_friends_handle() -> FriendsHandle {
    FRIENDS_HANDLE.clone()
}

enum ActorMessage {
    Fetch,
    SetFriends(Vec<User>),
}

pub struct FriendsActor {
    pub friends: watch::Sender<Vec<User>>,
    receiver: mpsc::UnboundedReceiver<ActorMessage>,
}

impl FriendsActor {
    pub fn new() -> (Self, FriendsHandle) {
        let (sender, receiver) = mpsc::unbounded_channel::<ActorMessage>();

        let (update_friends, watcher_friends) = watch::channel::<Vec<User>>(Vec::new());

        let friends_actor = Self {
            friends: update_friends,
            receiver,
        };

        let friends_handle = FriendsHandle::new(watcher_friends, sender);

        (friends_actor, friends_handle)
    }

    pub fn run(friends_actor: Self) {
        info!("Started friends actor event loop");
        spawn(friends_actor.event_loop());
    }

    async fn event_loop(mut self) {
        while let Some(message) = self.receiver.recv().await {
            match message {
                ActorMessage::Fetch => {
                    info!("FriendsActor ActorMessage::Fetch");
                    FriendsActor::fetch();
                }
                ActorMessage::SetFriends(friends) => {
                    info!("FriendsActor ActorMessage::SetFriends");
                    let _ = self.friends.send(friends);
                }
            }
        }
    }

    fn fetch() {
        info!("Fetch friends");
        spawn(async move {
            let (sender, receiver) = oneshot::channel::<Result<GetFriendsResponse, HttpError>>();
            HttpClientHandle::do_authenticated_request(
                Method::GET,
                "/users/friends/",
                None,
                None,
                sender,
            );

            match receiver.await.unwrap() {
                Ok(response) => {
                    info!("Fetch friends was success");
                    let friends_handle: FriendsHandle = use_friends_handle();
                    friends_handle.set_friends(response.result);
                }
                Err(http_error) => error!("Fetch friends error: {:?}", http_error),
            }
        });
    }
}

#[derive(Clone)]
pub struct FriendsHandle {
    pub friends: watch::Receiver<Vec<User>>,
    sender: mpsc::UnboundedSender<ActorMessage>,
}

impl FriendsHandle {
    fn new(
        friends: watch::Receiver<Vec<User>>,
        sender: mpsc::UnboundedSender<ActorMessage>,
    ) -> Self {
        Self { friends, sender }
    }

    pub fn fetch(&self) {
        let _ = self.sender.send(ActorMessage::Fetch);
    }

    pub fn reset_state(&self) {
        info!("Resetted state");
        self.set_friends(Vec::new());
    }

    pub fn get_friends(&mut self) -> Vec<User> {
        self.friends.borrow_and_update().clone()
    }

    pub fn set_friends(&self, friends: Vec<User>) {
        let _ = self.sender.send(ActorMessage::SetFriends(friends));
    }

    pub fn get_friends_filtered_blocked(&mut self) -> Vec<User> {
        let friends: Vec<User> = self.get_friends();
        let blocked_user_ids: Vec<usize> = use_blocked_users_handle()
            .get_blocked_users()
            .into_iter()
            .map(|user: User| user.id)
            .collect();
        let filtered_friends: Vec<User> = friends
            .into_iter()
            .filter(|user: &User| !blocked_user_ids.contains(&user.id))
            .collect();

        filtered_friends
    }

    pub fn is_friend(&mut self, user: &User) -> bool {
        let friend_user_ids: Vec<usize> = self
            .get_friends()
            .into_iter()
            .map(|user: User| user.id)
            .collect();
        let is_friend: bool = friend_user_ids.contains(&user.id);
        is_friend
    }

    pub fn unfriend_user(
        user_id: usize,
        sender: oneshot::Sender<Result<RemoveFriendResponse, HttpError>>,
    ) {
        HttpClientHandle::do_authenticated_request(
            Method::DELETE,
            format!("/users/friends/{user_id}"),
            None,
            None,
            sender,
        );
    }
}
