use crate::actors::{
    http_client_actor::use_http_client_handle, socket_client_actor::SocketClientHandle,
};
use crate::core::task::spawn;
use crate::lib::socket_event::SocketEvent;
use eframe::egui::Context;
use eframe::wasm_bindgen::{closure::Closure, JsCast};
use log::{error, info};
use tokio::sync::{mpsc, watch};
use web_sys::{js_sys, CloseEvent, ErrorEvent, MessageEvent, WebSocket};

pub enum ActorMessage {
    Reconnect,
    EmitSocketEvent(SocketEvent),
    HandleReceivedSocketEvent(SocketEvent),
}

pub struct SocketClientActor {
    socket: WebSocket,
    is_connected: watch::Receiver<bool>,
    receiver: mpsc::UnboundedReceiver<ActorMessage>,
}

impl SocketClientActor {
    pub fn new() -> (Self, SocketClientHandle) {
        let (sender, receiver) = mpsc::unbounded_channel::<ActorMessage>();
        let (update_is_connected, watcher_is_connected) = watch::channel::<bool>(false);

        let socket = SocketClientActor::connect();

        SocketClientActor::socket_event_loop(&socket, update_is_connected);

        let socket_client_actor = Self {
            socket,
            is_connected: watcher_is_connected,
            receiver,
        };
        let socket_client_handle = SocketClientHandle::new(sender);

        (socket_client_actor, socket_client_handle)
    }

    pub fn run(socket_client_actor: Self) {
        info!("Started SocketClientActor event loop");
        spawn(socket_client_actor.event_loop());
    }

    pub fn connect() -> WebSocket {
        info!("Connect to socket server");
        let host: String = "localhost:4000".to_string();
        let uri: String = format!("ws://{host}/ws/");

        WebSocket::new(&uri).unwrap()
    }

    async fn event_loop(mut self) {
        let _ = self.is_connected.wait_for(|is: &bool| is == &true).await;

        while let Some(message) = self.receiver.recv().await {
            match message {
                ActorMessage::Reconnect => {
                    info!("SocketClientActor ActorMessage::Reconnect");
                    let socket = SocketClientActor::connect();
                    self.socket = socket;
                }
                ActorMessage::EmitSocketEvent(socket_event) => {
                    info!("SocketClientActor ActorMessage::EmitSocketEvent");
                    crate::core::notification::create_toast(
                        crate::core::notification::NotificationKind::Error,
                        "emitted socket event",
                        // format!("emitted generic socket event: {}", &socket_event)
                    );
                    // let _ = self
                    //     .socket
                    //     .send_with_str(&socket_event);
                }
                ActorMessage::HandleReceivedSocketEvent(socket_event) => {
                    info!("SocketClientActor ActorMessage::HandleReceivedSocketEvent");
                    todo!();
                }
            }
        }
    }

    // fn socket_event_loop(socket: &WebSocket, actor_handle: mpsc::UnboundedSender<ActorMessage>) {
    fn socket_event_loop(socket: &WebSocket, update_is_connected: watch::Sender<bool>) {
        let onmessage_callback = Closure::<dyn FnMut(_)>::new(move |message: MessageEvent| {
            // if let Ok(abuf) = message.data().dyn_into::<js_sys::ArrayBuffer>() {
            //     let msg = format!("message event, received arraybuffer: {:?}", abuf);
            //     crate::core::notification::create_toast(crate::core::notification::NotificationKind::Error, &msg);
            // } else if let Ok(blob) = message.data().dyn_into::<web_sys::Blob>() {
            //     let msg = format!("message event, received blob: {:?}", blob);
            //     crate::core::notification::create_toast(crate::core::notification::NotificationKind::Error, &msg);
            // }

            if let Ok(txt) = message.data().dyn_into::<js_sys::JsString>() {
                info!("Got message: {:?}", &txt);

                let msg = format!("message event, received Text: {:?}", txt);
                crate::core::notification::create_toast(
                    crate::core::notification::NotificationKind::Error,
                    &msg,
                );
            }

            // else {
            //     let msg = format!("message event, received Unknown: {:?}", message.data());
            //     crate::core::notification::create_toast(crate::core::notification::NotificationKind::Error, &msg);
            // }
        });

        let onopen_callback = Closure::<dyn FnMut()>::new(move || {
            info!("Connected to socket server");

            crate::core::notification::create_toast(
                crate::core::notification::NotificationKind::Error,
                "connected",
            );
            let _ = update_is_connected.send(true);
        });

        // let onerror_callback = Closure::<dyn FnMut(_)>::new(move |e: ErrorEvent| {
        //     crate::core::notification::create_toast(crate::core::notification::NotificationKind::Error, "error happened");
        // });

        let onclose_callback = Closure::<dyn FnMut(_)>::new(move |e: CloseEvent| {
            error!("Closed connection to socket server");

            crate::core::notification::create_toast(
                crate::core::notification::NotificationKind::Error,
                "close happened",
            );
            // let _ = update_is_connected.send(false); // TODO
        });

        socket.set_onmessage(Some(onmessage_callback.as_ref().unchecked_ref()));
        socket.set_onopen(Some(onopen_callback.as_ref().unchecked_ref()));
        // socket.set_onerror(Some(onerror_callback.as_ref().unchecked_ref()));
        socket.set_onclose(Some(onclose_callback.as_ref().unchecked_ref()));
        onmessage_callback.forget();
        onopen_callback.forget();
        // onerror_callback.forget();
        onclose_callback.forget();
    }
}
