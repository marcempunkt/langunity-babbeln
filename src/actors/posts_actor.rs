use crate::actors::{
    app_actor::use_app_handle,
    blocked_users_actor::use_blocked_users_handle,
    http_client_actor::{use_http_client_handle, HttpClientHandle, HttpError},
};
use crate::core::models::{Post, User};
use crate::core::responses::GetPostsResponse;
use crate::core::task::spawn;
use log::{error, info};
use reqwest::Method;
use std::sync::LazyLock;
use tokio::sync::{mpsc, oneshot, watch};

pub static POSTS_HANDLE: LazyLock<PostsHandle> = LazyLock::new(|| {
    info!("Created POSTS_HANDLE");
    let (posts_actor, posts_handle) = PostsActor::new();
    PostsActor::run(posts_actor);
    posts_handle
});

pub fn use_posts_handle() -> PostsHandle {
    POSTS_HANDLE.clone()
}

enum ActorMessage {
    Fetch,
    SetPosts(Vec<Post>),
}

pub struct PostsActor {
    pub posts: watch::Sender<Vec<Post>>,
    receiver: mpsc::UnboundedReceiver<ActorMessage>,
}

impl PostsActor {
    pub fn new() -> (Self, PostsHandle) {
        let (sender, receiver) = mpsc::unbounded_channel::<ActorMessage>();

        let (update_posts, watcher_posts) = watch::channel::<Vec<Post>>(Vec::new());

        let posts_actor = Self {
            posts: update_posts,
            receiver,
        };

        let posts_handle = PostsHandle::new(watcher_posts, sender);

        (posts_actor, posts_handle)
    }

    pub fn run(posts_actor: Self) {
        info!("Started posts actor event loop");
        spawn(posts_actor.event_loop());
    }

    async fn event_loop(mut self) {
        while let Some(message) = self.receiver.recv().await {
            match message {
                ActorMessage::Fetch => {
                    info!("PostsActor ActorMessage::Fetch");
                    PostsActor::fetch();
                }
                ActorMessage::SetPosts(posts) => {
                    info!("PostsActor ActorMessage::SetPosts");
                    let _ = self.posts.send(posts);
                }
            }
        }
    }

    fn fetch() {
        info!("Fetch posts");
        spawn(async move {
            let (sender, receiver) = oneshot::channel::<Result<GetPostsResponse, HttpError>>();
            HttpClientHandle::do_authenticated_request(Method::GET, "/posts/", None, None, sender);

            match receiver.await.unwrap() {
                Ok(response) => {
                    info!("Fetch posts was successfull");
                    let posts_handle: PostsHandle = use_posts_handle();
                    posts_handle.set_posts(response.result);
                }
                Err(http_error) => error!("Fetch posts error: {:?}", http_error),
            }
        });
    }
}

#[derive(Clone)]
pub struct PostsHandle {
    pub posts: watch::Receiver<Vec<Post>>,
    sender: mpsc::UnboundedSender<ActorMessage>,
}

impl PostsHandle {
    fn new(posts: watch::Receiver<Vec<Post>>, sender: mpsc::UnboundedSender<ActorMessage>) -> Self {
        Self { posts, sender }
    }

    pub fn fetch(&self) {
        let _ = self.sender.send(ActorMessage::Fetch);
    }

    pub fn reset_state(&self) {
        info!("Resetted state");
        self.set_posts(Vec::new());
    }

    pub fn get_posts(&mut self) -> Vec<Post> {
        self.posts.borrow_and_update().clone()
    }

    pub fn get_posts_filtered_blocked(&mut self) -> Vec<Post> {
        let posts: Vec<Post> = self.get_posts();
        let blocked_user_ids: Vec<usize> = use_blocked_users_handle()
            .get_blocked_users()
            .into_iter()
            .map(|user: User| user.id)
            .collect();
        let filtered_posts: Vec<Post> = posts
            .into_iter()
            .filter(|post: &Post| {
                for user_id in &blocked_user_ids {
                    if post.author.contains(&format!("/users/{user_id}")) {
                        return false;
                    }
                }
                true
            })
            .collect();
        filtered_posts
    }

    pub fn set_posts(&self, posts: Vec<Post>) {
        let _ = self.sender.send(ActorMessage::SetPosts(posts));
    }
}
