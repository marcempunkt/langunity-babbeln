use crate::actors::{
    http_client_actor::use_http_client_handle,
    messages_actor::use_messages_handle,
    socket_client_actor::{use_socket_client_handle, SocketClientHandle},
};
use crate::core::task::spawn;
use crate::lib::socket_event::{SocketAction, SocketEvent};
use eframe::egui::Context;
use futures_util::SinkExt;
use futures_util::{
    stream::{SplitSink, SplitStream},
    StreamExt,
};
use log::{error, info};
use std::sync::LazyLock;
use tokio::net::TcpStream;
use tokio::sync::{mpsc, watch};
use tokio_tungstenite::{
    tungstenite::http::{header, Request},
    tungstenite::protocol::Message,
    MaybeTlsStream, WebSocketStream,
};

type StreamWrite = SplitSink<WebSocketStream<MaybeTlsStream<TcpStream>>, Message>;
type StreamRead = SplitStream<WebSocketStream<MaybeTlsStream<TcpStream>>>;

pub enum ActorMessage {
    SetStream((StreamWrite, StreamRead)),
    Reconnect,
    HandleReceivedSocketEvent(SocketEvent),
    EmitSocketEvent(SocketEvent),
}

pub struct SocketClientActor {
    stream: (Option<StreamWrite>, Option<StreamRead>),
    receiver: mpsc::UnboundedReceiver<ActorMessage>,
}

impl SocketClientActor {
    pub fn new() -> (Self, SocketClientHandle) {
        let (sender, receiver) = mpsc::unbounded_channel::<ActorMessage>();

        SocketClientActor::connect();

        let socket_client_actor = Self {
            stream: (None, None),
            receiver,
        };
        let socket_client_handle = SocketClientHandle::new(sender);

        (socket_client_actor, socket_client_handle)
    }

    pub fn run(socket_client_actor: Self) {
        info!("Started SocketClientActor event loop");
        spawn(socket_client_actor.event_loop());
    }

    pub fn connect() {
        spawn(async move {
            info!("Connect to socket server");

            let host: String = "localhost:4000".to_string();
            let uri: String = format!("ws://{host}/ws/");

            let mut http_client_handle = use_http_client_handle();
            let access_token: String = http_client_handle.get_access_token_async().await;

            let request = Request::builder()
                .uri(uri)
                .header(header::HOST, host)
                .header(header::AUTHORIZATION, &format!("Bearer {}", &access_token))
                .header(header::UPGRADE, "websocket")
                .header(header::CONNECTION, "Upgrade")
                .header(header::SEC_WEBSOCKET_KEY, "pDTDUR/eEFpXHb847ddH1g==")
                .header(header::SEC_WEBSOCKET_VERSION, "13")
                .body(())
                .unwrap();

            let (socket, _) = tokio_tungstenite::connect_async(request)
                .await
                .expect("Failed to connect");
            let (write, read) = socket.split();

            let _ = use_socket_client_handle()
                .sender
                .send(ActorMessage::SetStream((write, read)));
        });
    }

    async fn event_loop(mut self) {
        while let Some(message) = self.receiver.recv().await {
            match message {
                ActorMessage::SetStream((write, read)) => {
                    info!("SocketClientActor ActorMessage::SetStream");
                    self.stream = (Some(write), Some(read));

                    spawn(async move {
                        SocketClientActor::socket_event_loop(self.stream.1.unwrap()).await;
                    });
                }
                ActorMessage::Reconnect => {
                    info!("SocketClientActor ActorMessage::Reconnect");
                    SocketClientActor::connect();
                }
                ActorMessage::HandleReceivedSocketEvent(socket_event) => {
                    info!("SocketClientActor ActorMessage::HandleReceivedSocketEvent");
                    SocketClientActor::handle_received_socket_event(socket_event);
                }
                ActorMessage::EmitSocketEvent(socket_event) => {
                    info!("SocketClientActor ActorMessage::EmitSocketEvent");
                    let stream: &mut StreamWrite = self.stream.0.as_mut().unwrap();

                    SocketClientActor::emit_socket_event(stream, socket_event).await;
                }
            }
        }
    }

    async fn handle_received_socket_event(socket_event: SocketEvent) {
        match socket_event.action {
            // Users
            SocketAction::DeleteUser => {}
            SocketAction::UpdateUserStatus => {}
            SocketAction::UpdateUserName => {}
            SocketAction::UpdateUserAvatar => {}
            // Users - Friends
            SocketAction::RemoveFriend => {}
            // Users - BlockedUsers
            SocketAction::BlockUser => {}
            SocketAction::UnblockUser => {}
            // Friendrequests
            SocketAction::SendFriendrequest => {}
            SocketAction::RemoveFriendrequest => {}
            SocketAction::AcceptFriendrequest => {}
            SocketAction::DeclineFriendrequest => {}
            // Posts
            SocketAction::CreatePost => {}
            SocketAction::RemovePost => {}
            SocketAction::LikePost => {}
            SocketAction::UnlikePost => {}
            SocketAction::CommentPost => {}
            SocketAction::RemoveCommentPost => {}
            // Message
            SocketAction::SendMessage => {
                use_messages_handle().fetch_chat(socket_event.payload.from_user_id.unwrap());
            }
            SocketAction::UpdateMessage => {}
            SocketAction::UnsendMessage => {}
            SocketAction::RemoveMessage => {}
            SocketAction::DeleteChat => {}
        }
    }

    async fn emit_socket_event(stream: &mut StreamWrite, socket_event: SocketEvent) {
        info!("Emit socket event: {:?}", socket_event);
        let socket_event: String = serde_json::to_string(&socket_event).unwrap();
        let _ = stream.send(Message::Text(socket_event)).await;
    }

    async fn socket_event_loop(stream_read: StreamRead) {
        info!("Started socket event loop");
        stream_read
            .for_each(|message| async {
                info!("Got socket event");
                match message {
                    Ok(Message::Text(txt)) => {
                        use_socket_client_handle().handle_received_socket_event(txt);
                    }
                    Ok(Message::Close(_)) => {
                        use_socket_client_handle().reconnect();
                    }
                    Err(e) => error!("Socket Event Loop Error: {e}"),
                    // Ok(Message::Binary(bin)) => println!("Received binary data"),
                    // Ok(Message::Ping(_)) => println!("Received ping"),
                    // Ok(Message::Pong(_)) => println!("Received pong"),
                    _ => (), // Handle other message types as needed
                }
            })
            .await;
    }
}
