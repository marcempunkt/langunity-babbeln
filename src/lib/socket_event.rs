use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
pub struct SocketEvent {
    pub action: SocketAction,
    pub payload: SocketPayload,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Copy, Clone)]
pub enum SocketAction {
    // Users
    DeleteUser,
    UpdateUserStatus,
    UpdateUserName,
    UpdateUserAvatar,
    // Users - Friends
    RemoveFriend,
    // Users - BlockedUsers
    BlockUser,
    UnblockUser,
    // Friendrequests
    SendFriendrequest,
    RemoveFriendrequest,
    AcceptFriendrequest,
    DeclineFriendrequest,
    // Posts
    CreatePost,
    RemovePost,
    LikePost,
    UnlikePost,
    CommentPost,
    RemoveCommentPost,
    // Message
    SendMessage,
    UpdateMessage,
    UnsendMessage,
    RemoveMessage,
    DeleteChat,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Default)]
pub struct SocketPayload {
    pub from_user_id: Option<usize>,
    pub to_user_id: Option<usize>,
    pub reference_object_id: Option<usize>,
}
