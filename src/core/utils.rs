use std::time::Duration;
use tokio::sync::oneshot;

/// Simple helper function to handle a oneshot receiver
/// inside the eframe render loop
pub fn handle_oneshot_receiver<T>(
    maybe_receiver: &mut Option<oneshot::Receiver<T>>,
    on_success: impl FnOnce(T),
) {
    if let Some(receiver) = maybe_receiver {
        match receiver.try_recv() {
            Ok(t) => on_success(t),
            Err(oneshot::error::TryRecvError::Closed) => *maybe_receiver = None,
            Err(oneshot::error::TryRecvError::Empty) => (),
        }
    }
}

/// Wait/sleep inside the current thread
/// for a passed in duration
#[cfg(not(target_arch = "wasm32"))]
pub async fn wait_for(duration: Duration) {
    tokio::time::sleep(duration).await;
}

/// Same as the rust version of wait_for.
/// Creates web_sys/Rust version of this:
/// new Promise((resolve) => setTimeout(() => resolve(null), duration);
#[cfg(target_arch = "wasm32")]
pub async fn wait_for(duration: Duration) {
    // use eframe::wasm_bindgen::{JsCast, JsValue, closure::Closure};
    let duration: u32 = duration.as_millis() as u32;
    gloo_timers::future::TimeoutFuture::new(duration).await;

    // let promise = js_sys::Promise::new(&mut |resolve, _reject| {
    //     gloo_timers::callback::Timeout::new(
    //         &Closure::<dyn FnMut()>::new(move || {
    //             resolve.call0(&JsValue::undefined()).unwrap();
    //         }).as_ref().unchecked_ref(),
    //         duration,
    //     );
    //     // web_sys::window()
    //     //     .unwrap()
    //     //     .set_timeout_with_callback_and_timeout_and_arguments_0(
    //     //         &Closure::<dyn FnMut()>::new(move || {
    //     //             resolve.call0(&JsValue::undefined()).unwrap();
    //     //         }).as_ref().unchecked_ref(),
    //     //         duration,
    //     //     )
    //     //     .unwrap();
    // });
    // wasm_bindgen_futures::JsFuture::from(promise).await.unwrap();
}
