use egui_notify::{Toast, Toasts};
use std::ptr;
use std::sync::Once;
use std::time::Duration;

static INIT: Once = Once::new();
static mut TOASTS: *mut Toasts = ptr::null_mut();

pub fn use_toasts() -> &'static mut Toasts {
    unsafe {
        INIT.call_once(|| {
            TOASTS = Box::into_raw(Box::new(Toasts::new()));
        });
        &mut *TOASTS
    }
}

pub enum NotificationKind {
    Info,
    Success,
    Warning,
    Error,
}

pub fn create_toast(kind: NotificationKind, content: impl Into<String>) {
    let content: String = content.into();
    let toasts: &mut Toasts = use_toasts();
    let toast: &mut Toast = match kind {
        NotificationKind::Info => toasts.info(content),
        NotificationKind::Success => toasts.success(content),
        NotificationKind::Warning => toasts.warning(content),
        NotificationKind::Error => toasts.error(content),
    };
    toast.set_duration(Some(Duration::from_secs(10)));
}

#[cfg(not(target_arch = "wasm32"))]
pub fn create_notification(kind: NotificationKind, content: impl Into<String>) {
    todo!()
}

#[cfg(target_arch = "wasm32")]
pub fn create_notification(kind: NotificationKind, content: impl Into<String>) {
    todo!()
}
