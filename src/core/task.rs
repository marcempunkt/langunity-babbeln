use std::future::Future;

#[cfg(not(target_arch = "wasm32"))]
pub fn spawn<F>(async_closure: F)
where
    F: Future<Output = ()> + Send + 'static,
{
    tokio::spawn(async_closure);
}

#[cfg(target_arch = "wasm32")]
pub fn spawn<F>(async_closure: F)
where
    F: Future<Output = ()> + 'static,
{
    wasm_bindgen_futures::spawn_local(async_closure);
}

#[cfg(not(target_arch = "wasm32"))]
pub fn spawn_blocking<F>(f: F)
where
    F: FnOnce() + Send + 'static,
{
    tokio::task::spawn_blocking(f);
}

#[cfg(target_arch = "wasm32")]
pub fn spawn_blocking<F>(f: F)
where
    F: FnOnce() + Send + 'static,
{
    // TODO wasm doesnt support threading yet and web workers are to
    // difficult for me to integrate so for now i dont care for
    // web perfomance which makes me very sad ;(
    spawn(async move {
        f();
    });

    // let worker = web_sys::Worker::new("./worker.js").unwrap();
    // let ptr = Box::into_raw(Box::new(Box::new(f) as Box<dyn FnOnce()>));
    // let msg = js_sys::Array::new();
    // msg.push(&wasm_bindgen::memory());
    // msg.push(&wasm_bindgen::JsValue::from(ptr as u32));
    // let _ = worker.post_message(&msg);
}

// #[cfg(target_arch = "wasm32")]
// #[wasm_bindgen::prelude::wasm_bindgen]
// pub fn worker_entry_point(ptr: u32) {
//   let closure = unsafe { Box::from_raw(ptr as *mut Box<dyn FnOnce()>) };
//   (*closure)();
// }
