use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct User {
    pub id: usize,
    pub username: String,
    pub register_date: DateTime<Utc>,
    pub status: String,
    pub avatar: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Friendrequest {
    pub id: usize,
    pub from_user: String,
    pub to_user: String,
    pub accept: String,
    pub decline: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Message {
    pub id: usize,
    pub sender: String,
    pub receiver: String,
    pub content: String,
    pub send_at: DateTime<Utc>,
    pub edited_at: Option<DateTime<Utc>>,
    pub is_deleted_by_sender: bool,
    pub is_deleted_by_receiver: bool,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Post {
    pub id: usize,
    pub author: String,
    pub content: String,
    pub created_at: DateTime<Utc>,
    pub likes: Vec<String>,
    pub images: Vec<String>,
    pub comments: Vec<PostComment>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct PostComment {
    pub id: usize,
    pub author: String,
    pub content: String,
    pub created_at: DateTime<Utc>,
}
