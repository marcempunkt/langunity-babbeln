use crate::core::models::{Friendrequest, Message, Post, User};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct LoginResponse {
    pub access: String,
    pub refresh: String,
}

#[derive(Serialize, Deserialize)]
pub struct RefreshTokensResponse {
    pub access: String,
    pub refresh: String,
}

#[derive(Serialize, Deserialize)]
pub struct RegisterResponse {
    pub message: String,
    pub result: User,
}

#[derive(Serialize, Deserialize)]
pub struct GetUserResponse {
    pub message: String,
    pub result: User,
}

#[derive(Serialize, Deserialize)]
pub struct SendFriendrequestResponse {
    pub message: String,
    pub result: Friendrequest,
}

#[derive(Serialize, Deserialize)]
pub struct AcceptFriendrequestResponse {
    message: String,
    result: User,
}

#[derive(Serialize, Deserialize)]
pub struct DeclineFriendrequestResponse {
    message: String,
}

#[derive(Serialize, Deserialize)]
pub struct DeleteFriendrequestResponse {
    message: String,
}

#[derive(Serialize, Deserialize)]
pub struct BlockUserResponse {
    message: String,
    result: User,
}

#[derive(Serialize, Deserialize)]
pub struct UnblockUserResponse {
    message: String,
}

#[derive(Serialize, Deserialize)]
pub struct RemoveFriendResponse {
    message: String,
}

#[derive(Serialize, Deserialize)]
pub struct SendMessageResponse {
    message: String,
    result: Message,
}

#[derive(Serialize, Deserialize)]
pub struct ListResponse<T> {
    pub count: usize,
    pub next: Option<String>,
    pub previous: Option<String>,
    pub result: Vec<T>,
}

pub type GetUsersResponse = ListResponse<User>;
pub type GetMessagesResponse = ListResponse<Message>;
pub type GetChatResponse = ListResponse<Message>;
pub type GetFriendsResponse = ListResponse<User>;
pub type GetBlockedUsersResponse = ListResponse<User>;
pub type GetChatPartnersResponse = ListResponse<User>;
pub type GetFriendrequestsResponse = ListResponse<Friendrequest>;
pub type GetPostsResponse = ListResponse<Post>;
