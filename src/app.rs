use crate::actors::{
    app_actor::{use_app_handle, AppHandle},
    http_client_actor::{use_http_client_handle, HttpClientHandle},
    socket_client_actor::use_socket_client_handle,
};
use crate::core::notification::use_toasts;
use crate::windows::{login_register::LoginRegister, main::Main};
use eframe::{CreationContext, Frame, Storage};
use egui::Context;
use log::info;

pub struct App {
    app_handle: AppHandle,
    http_client_handle: HttpClientHandle,
    main: Main,
    login_register: LoginRegister,
}

impl App {
    pub fn new(cc: &CreationContext<'_>) -> Self {
        let http_client_handle = use_http_client_handle();
        let storage: &dyn Storage = cc.storage.expect("No Storage Support");

        if let Some(access_token) = storage.get_string("access_token") {
            if !access_token.is_empty() {
                info!("Found access_token to Storage");
                http_client_handle.set_access_token(Some(access_token));
            }
        }

        if let Some(refresh_token) = storage.get_string("refresh_token") {
            if !refresh_token.is_empty() {
                info!("Found refresh_token to Storage");
                http_client_handle.set_refresh_token(Some(refresh_token));
            }
        }

        Self {
            app_handle: use_app_handle(),
            http_client_handle,
            main: Main::new(),
            login_register: LoginRegister::new(),
        }
    }
}

impl eframe::App for App {
    fn save(&mut self, storage: &mut dyn Storage) {
        if let Some(token) = self.http_client_handle.get_access_token() {
            storage.set_string("access_token", token);
            info!("Saved access_token inside storage");
        } else {
            storage.set_string("access_token", String::new());
            info!("Saved access_token as empty String inside storage");
        }

        if let Some(token) = self.http_client_handle.get_refresh_token() {
            storage.set_string("refresh_token", token);
            info!("Saved access_token inside storage");
        } else {
            storage.set_string("refresh_token", String::new());
            info!("Saved refresh_token as empty String inside storage");
        }

        storage.flush();
    }

    fn update(&mut self, ctx: &Context, _: &mut Frame) {
        self.app_handle.set_ctx(ctx.clone());

        use_toasts().show(ctx);

        let access: Option<String> = self.http_client_handle.get_access_token();
        let refresh: Option<String> = self.http_client_handle.get_refresh_token();
        let is_logged_in: bool = access.is_some() && refresh.is_some();

        if is_logged_in {
            self.main.render(ctx);
        } else {
            self.login_register.render(ctx);
        }
    }
}
