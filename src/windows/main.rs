use crate::actors::{
    app_actor::{use_app_handle, AppHandle},
    http_client_actor::HttpClientHandle,
    socket_client_actor::use_socket_client_handle,
    user_actor::{use_user_handle, UserHandle},
};
use crate::components::{
    chat::Chat, dashboard::Dashboard, direct_messages::DirectMessages, friends::Friends,
    timeline::Timeline,
};
use crate::core::task::spawn;
use egui::{Context, Ui};
use log::info;

pub struct Main {
    app_handle: AppHandle,
    user_handle: UserHandle,
    is_mounted: bool,
    dashboard: Dashboard,
    friends: Friends,
    timeline: Timeline,
    chat: Chat,
    direct_messages: DirectMessages,
}

impl Main {
    pub fn new() -> Self {
        Self {
            app_handle: use_app_handle(),
            user_handle: use_user_handle(),
            is_mounted: false,
            dashboard: Dashboard::new(),
            friends: Friends::new(),
            timeline: Timeline::new(),
            chat: Chat::new(),
            direct_messages: DirectMessages::new(),
        }
    }

    pub fn render(&mut self, ctx: &Context) {
        if !self.is_mounted {
            info!("Main component mounted for the \"first\" time");
            self.is_mounted = true;
            HttpClientHandle::fetch_all();
            // let _connect = use_socket_client_handle(); // DEBUG
            //                                            // _connect.ping();
            //                                            // _connect.emit("some socket event".to_string());
        }

        egui::SidePanel::left("main_left_panel").show(ctx, |ui| {
            ui.horizontal(|ui: &mut Ui| {
                if let Some(me) = self.user_handle.get_me() {
                    ui.heading(&format!("{:?}", me.username));
                }

                if ui.button("logout").clicked() {
                    info!("Sidebar's logout button clicked");
                    *self = Self::new();
                    HttpClientHandle::reset_all();
                }
            });

            if ui.button("dashboard").clicked() {
                info!("Sidebar's dashboard button clicked");
                self.app_handle.set_show_dashboard();
            }

            if ui.button("friends").clicked() {
                info!("Sidebar's friends button clicked");
                self.app_handle.set_show_friends();
            }

            if ui.button("timeline").clicked() {
                info!("Sidebar's timeline button clicked");
                self.app_handle.set_show_timeline();
            }

            self.direct_messages.render(ui, ctx);
        });

        egui::CentralPanel::default().show(ctx, |ui: &mut Ui| {
            if self.app_handle.get_show_dashboard() {
                self.dashboard.render(ui, ctx);
            } else if self.app_handle.get_show_friends() {
                self.friends.render(ui, ctx);
            } else if self.app_handle.get_show_timeline() {
                self.timeline.render(ui, ctx);
            } else if self.app_handle.get_show_chat().is_some() {
                self.chat.render(ui, ctx);
            }
        });
    }
}
