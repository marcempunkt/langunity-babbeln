use crate::components::{login::Login, register::Register};
use egui::{Context, Ui};
use egui_notify::Toasts;
use std::cell::RefCell;
use std::rc::Rc;

pub struct LoginRegister {
    login: Login,
    register: Register,
    show_login: Rc<RefCell<bool>>,
}

impl LoginRegister {
    pub fn new() -> Self {
        let show_login = Rc::new(RefCell::new(true));

        Self {
            login: Login::new(),
            register: Register::new(Rc::clone(&show_login)),
            show_login: Rc::clone(&show_login),
        }
    }

    pub fn render(&mut self, ctx: &Context) {
        egui::CentralPanel::default().show(ctx, |ui: &mut Ui| {
            if ui.button("login").clicked() {
                *self.show_login.borrow_mut() = true;
            }

            if ui.button("i need an account!").clicked() {
                *self.show_login.borrow_mut() = false;
            }

            if *self.show_login.borrow() {
                self.login.render(ui, ctx);
            } else {
                self.register.render(ui, ctx);
            }
        });
    }
}
