#![feature(lazy_cell)]
#![warn(clippy::all, rust_2018_idioms)]
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")] // hide console window on Windows in release

mod actors;
mod app;
mod components;
mod core;
mod lib;
mod modals;
mod windows;

use app::App;

#[cfg(not(target_arch = "wasm32"))]
#[tokio::main(flavor = "multi_thread", worker_threads = 10)]
async fn main() {
    env_logger::Builder::from_env(
        env_logger::Env::default().filter_or("BABBELN_LOG_LEVEL", "info"),
    )
    .init();

    let native_options = eframe::NativeOptions {
        // TODO schau dir die Options an
        // initial_window_size: Some([400.0, 300.0].into()),
        // min_window_size: Some([300.0, 220.0].into()),
        ..Default::default()
    };

    eframe::run_native(
        "babbeln",
        native_options,
        Box::new(|cc| {
            egui_extras::install_image_loaders(&cc.egui_ctx);
            Box::new(App::new(cc))
        }),
    )
    .unwrap();
}

#[cfg(target_arch = "wasm32")]
#[tokio::main(flavor = "current_thread")]
async fn main() {
    eframe::WebLogger::init(log::LevelFilter::Debug).ok();

    let web_options = eframe::WebOptions::default(); // TODO auch hier was ist alles möglich

    wasm_bindgen_futures::spawn_local(async {
        eframe::WebRunner::new()
            .start("app", web_options, Box::new(|cc| Box::new(App::new(cc))))
            .await
            .unwrap();
    });
}
